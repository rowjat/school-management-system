<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id'); //registion No
            $table->integer('application_id');
            $table->string('name');
            $table->string('f_name');
            $table->string('m_name');
            $table->date('birth_date');
            $table->string('gender');
            $table->date('date');
            $table->integer('school_class_id');
            $table->string('contact_no');
            $table->string('email');
            $table->text('address');
            $table->string('birth_place');
            $table->string('nationality');
            $table->string('religion');
            $table->string('pictur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}
