<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeheadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeheads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('session_year_id');
            $table->integer('school_class_id');
            $table->string('name');
            $table->double('amount');
            $table->integer('collection_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feeheads');
    }
}
