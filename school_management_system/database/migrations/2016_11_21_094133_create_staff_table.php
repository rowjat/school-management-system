<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('f_name');
            $table->string('m_name');
            $table->string('gender');
            $table->date('dob');
            $table->string('nationality');
            $table->string('contact_no')->unique();
            $table->string('email')->unique();
            $table->text('address');
            $table->integer('degignation_id');
            $table->date('join_date');
            $table->string('statase');
            $table->date('releas_date');
            $table->string('pictur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staff');
    }
}
