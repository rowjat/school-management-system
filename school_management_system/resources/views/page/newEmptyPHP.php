@extends('master')
@section('title','Setting')
@section('content')
<nav class="navbar navbar-default navbar-fixed-top nav-1" role="navigation" id="navbar">
    <ul class="nav-2">
        <li><a href="{{route('class_create')}}" >Add New class</a></li>
    </ul>
</nav>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Exampale</h1>
    </div>
</div>
<div class="row">
    <div class="col col-md-8 ">
        {!! Form::open(['class'=>'form-horizontal','id'=>'form']) !!}
        <div class="form-group">
            {!! Form::label('session_years_id','Select Session :',['class'=>'col-sm-4 control-label']) !!}
            <div class="col-sm-4">
                <?php
                $session_years_arr = ['Select'];
                    foreach ($session_years as $session_year) {
                        $session_years_arr[$session_year->id] = $session_year->name;
                    }
                ?>

                {!! Form::select('session_years_id',$session_years_arr,null,['class'=>'form-control','id'=>'ajax_class_index'])!!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div><hr/>
<div class="row">
    <div class="col col-md-6 col-md-offset-3" id="showme">
        
    </div>
</div>

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
       /*
        * Start code
        */ 
        $('#ajax_class_index').on('change', function () {
                $.ajax({
                    url: "ajax_class_index",
                    type: "GET",
                    async: false,
                    data: $("#form").serialize(),
                    success: function (data) {
//                        console.log(data);
//                        
                         $("#showme").html(data);
                        }
                        
                });
            });
            $(document).on('click','.pagination a', function (e) {
                        e.preventDefault();
                        var page = $(this).attr('href').split('page=')[1]; 
                $.ajax({
                    url: "ajax_class_index_page?page="+page,
                    type: "GET",
                    async: false,
                    data: {'id':$('#ajax_class_index').val()},
                    success: function (data) {
                        console.log(data);
//                        
                         $("#showme").html(data);
                        }
                        
                });
            });
        alert($('#ajax_class_index').val());
       /*
        * End Code
        */
    });
    </script>
@endsection

