@extends('master')
@section('title','Fee')
@section('content')
<div class="page-header">
    <h4>Fee Collection</h4>
</div>
{!! Form::open(['method'=>'POST','route'=>'fee_collection_store','class'=>'form-horizontal']) !!}

<div id="foo"></div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group-sm">
            <label for="receipt_no" class="control-label col-md-6">Receipt No : </label>
            <div class="col-md-6">
                <span class="form-control">{{$profit+1}}</span><input type="hidden"  name="receipt_no" id="receipt_no"  value="{{$profit+1}}">
            </div>
        </div>
        <div class="form-group-sm">
            <label for="class_id" class="control-label col-md-6">Select Class :</label>
            <div class="col-md-6">
                <select name="class_id" id="class_id" class="form-control">
                    <option value=''>Select</option>
                    @foreach($class_id as $value)
                    <option value='{{$value->id}}'>{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group-sm">
            <label for="admission_id" class="control-label col-md-6">Select Student :</label>
            <div class="col-md-6">
                <select name="admission_id" id="admission_id" class="form-control">

                </select>
            </div>
        </div>
        <div class="form-group-sm">
            {!!Form::label('date','Date :',['class'=>'control-label col-md-6 '])!!}
            <div class="col-md-6">
                {!!Form::date('date',\Carbon\Carbon::now(),['class'=>'form-control'])!!}
            </div>
        </div>

        <div class="col-md-12 text-right" style="padding-top: 5px" id="submit">

        </div>
    </div>
    <div class="col-md-8" id="fee_set">


    </div> 
</div><br/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 " >
        <!--view-->
    </div>
</div>

{!! Form::close() !!}
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */
$('#class_id').on('change',function(){
                    $.ajax({
                        url: "fee_collection_create",
                        method: "get",
                        cache: false,
                        data: {
                            'class_id':$('#class_id').val(),
                            'i':0
                        },
                        success: function (data) {
                            var result = "<option value=''>Select</option>"
                                $.each(data,function(k,v){
                                   result +="<option value='"+v.id+"'>"+v.id+"</option>"; 
                                });
                                $('#submit').append().empty();
                                $('#fee_set').append().empty();
                            $('#admission_id').append().empty();
                            $('#admission_id').append(result);
                            console.log(data);
                        }
                    });
                });  
    var month_arr = ['Select Month','January','February','March','April','May','June','July','August','September','October','November','December'];         
$('#admission_id').on('change',function(){
                    $.ajax({
                        url: "fee_collection_create",
                        method: "get",
                        cache: false,
                        data: {
                            'admission_id':$('#admission_id').val(),
                            'i':1
                        },
                        success: function (data) {
                            $('#submit').append().empty();
                            $('#submit').append('<button type="submit" class="btn btn-sm btn-success">Save</button>');
                            $('#fee_set').append().empty();
                            $('#fee_set').append(data);
                            console.log(data);
                        }
                    });
                }); 
               
         $(document).on('change', '.month', function () {
             var id = $(this).attr('id');
                 row_id=$('#id_'+id).attr('class');
                 sub_total =Number($('#sub_total_'+row_id).text());
             if(this.checked){  
                 $('#row_'+id).append().empty();
                 $('#row_'+id).append('<input type="hidden" name="amount['+row_id+'][]" value="'+$('#balance_'+id).attr('class')+'">');
                 $('#sub_total_'+row_id).text(sub_total +Number($('#balance_'+id).attr('class')))
                 total = 0;
                $('.pay').each(function(){
                    total += Number($(this).text());
                });
                 $('#total').text(total);
        }else{
            $('#row_'+id).append().empty();
            $('#sub_total_'+row_id).text(sub_total - Number($('#balance_'+id).attr('class')))
            total = 0;
                $('.pay').each(function(){
                    total += Number($(this).text());
                });
                 $('#total').text(total);
        }
             console.log(sub_total);
             
         });
         $(document).on('change', '.input_amount', function () {
          
             var id = $(this).attr('id');
                balance = Number($('#balance_'+id).text());
                input = $(this).val();
                $('#pay_'+id).text(input);
                $('#net_balance_'+id).text(balance-input);
                 total = 0;
                $('.pay').each(function(){
                    total += Number($(this).text());
                });
                 $('#total').text(total);
                console.log(balance);
         });
            /*
             * End Code
             */
    });
</script>
@endsection

