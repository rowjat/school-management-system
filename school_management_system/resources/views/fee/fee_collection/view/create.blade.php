<div class="" style="border: 2px double #EEE;padding:40px" >
    <table class="fee-tbl" >

        <tr style="background-color: #B0BEC5">
            <th colspan="3">Feehead</th>
            <th>Balance</th>
            <th></th>
            <th>Pay</th>

        </tr>
        @php($i=0)
        @php($t=0)
        @foreach($fee_set as $value)

        @php($t+= round($value->amount/12))
        @if($value->feehead->collection_type ==1)
        @php($coll = $collection->where('fee_head_id',$value->feehead->id)->sum('amount'))
        @if($value->feehead->amount > $coll)
        @php($i++)
        <tr id="row_{{$i}}">
            <td colspan="3"><span class="badge"><i class="fa fa-asterisk" aria-hidden="true"></i>&nbsp;{{$value->feehead->name}}<input type="hidden" name="fee_head_id[]" value="{{$value->feehead_id}}"</span></td>
            <td id="balance_{{$i}}">
                <a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
            </td>
            <td>{{$i-1}}</td>
            <td style="text-align: center;font-weight: bold;"></td>


        </tr>
        <tbody class="collapse" id="collapseExample">
            @php($st = 0)
            @for($j=0;$j<12;$j++)

            @php($coll = $collection->where('fee_head_id',$value->feehead->id)->where('month',(int)date('m', strtotime("+" . $j . " months", strtotime($month))))->first())
            @if($coll==null)
            @php($st += round($value->amount/12))
            <tr>

                <td colspan="2"></td>
                <td >
                    <input  type="checkbox" class="month" id="{{$value->feehead->id.'_'.$j}}" name="month[{{$i-1}}][]" value="{{date('m', strtotime("+" . $j . " months", strtotime($month)))}}">
                    <label for="{{$value->feehead->id.'_'.$j}}" class="form-label" >{{date('M-Y', strtotime("+" . $j . " months", strtotime($month)))}}</label>
                    <div id="id_{{$value->feehead->id.'_'.$j}}" class="{{$i-1}}"></div>
                    <div id="balance_{{$value->feehead->id.'_'.$j}}" class="{{round($value->amount/12)}}"></div>
                    <div id="row_{{$value->feehead->id.'_'.$j}}">
                        <input type="hidden" name="month[{{$i-1}}][]" value="">
                        <input type="hidden" name="amount[{{$i-1}}][]" value="">
                    </div>
                </td>
                <td class="amount">{{round($value->amount/12)}}</td>
                <td></td>
                <td></td>  
            </tr>
            @endif
            @endfor

            <tr>
                <td colspan="2"></td>
                <td  style="border-top:1px solid;">Total</td>
                <td class="amount" style="border-top:1px solid;">{{$st}}</td>

                <td></td>
                <td class="amount pay" id="sub_total_{{$i-1}}">0</td>
            </tr>
        </tbody>
        @endif
        @else
        @php($coll = $collection->where('fee_head_id',$value->feehead->id)->sum('amount'))
        @php($amount = $coll == null?0:$coll )
        @if($amount < $value->feehead->amount)
        @php($i++)
        <tr id="row_{{$i}}">
            <td colspan="3">
                <span class="badge"><i class="fa fa-asterisk" aria-hidden="true"></i>&nbsp;{{$value->feehead->name}}</span>
                <input type="hidden" name="fee_head_id[]" value="{{$value->feehead_id}}"</td>
        <input type="hidden" name="month[{{$i-1}}][]" value="0"</td>
        <td class="amount" id="balance_{{$value->feehead->id}}">{{$value->feehead->amount - $amount}}</td>
        <td><input type="number" max="{{$value->amount}}" style="width:100px" class="form-control input_amount" id="{{$value->feehead->id}}" name="amount[{{$i-1}}][]"></td>
        <td class="amount pay" id="pay_{{$value->feehead->id}}">{{$coll}}</td>  
        </tr>
        @endif
        @endif
        <!-------------->

        <!------------------->
        @endforeach
        <tr id="total_row" style="border-top: solid;">
            <td ></td>
            <td>total</td>
            <td></td>
            <td></td>
            <td></td>
            <td class="amount" style="border-bottom: double #000" id="total">0</td>
        </tr>
    </table>

</div>

