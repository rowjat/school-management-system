@extends('master')
@section('title','Fee')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Feehead Entry Form</h2></div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','route'=>'feehead_store','class'=>'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('session_year_id','Session Year :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">

                        <?php
                        $session_arr = ['select'];
                        foreach ($session_years as $session_year) {
                            $session_arr[$session_year->id] = $session_year->name;
                        }
                        ?>
                        {!! Form::select('session_year_id',$session_arr,null,['class'=>'form-control','id'=>'session_year_id']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('school_class_id','Clase Name',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        <select name="school_class_id" id='school_class_id' class="form-control"> </select>

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('name','Name :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('collection_type','Type Of Collection :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::select('collection_type',[1=>'Monthly',2=>'Sission Wise',3=>'Only This'],null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('amount','Amount :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::number('amount',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group" style="width: ">
                    <div class="col-sm-offset-4 col-sm-10 ">
                        {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                        <a href="{{route('feehead_index')}}" class="btn btn-danger">Cancle</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
       $('#session_year_id').on('change',function(){
              $.ajax({
                    url: "feehead_create_1",
                    type: "GET",
                    data: {'id':$("#session_year_id").val()},
                    success: function (data) {
                        console.log(data);
                        $('#school_class_id').append(" <option value=''></option>");
                        if(data){
                        var option = ""; 
                        $.each(data,function(index,obj){
                             option += " <option value='"+obj.id+"'>"+obj.name+"</option>";  
                        });
                        $('#school_class_id').append(option).empty();
                        $('#school_class_id').append(option);
                        }else{
                            $('#school_class_id').append(option).empty();
                        }
                    }    
                });
       });
       
       /*
        * End Code
        */
    });
    </script>
@endsection

