<!--<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="background-image:url('img/banar.jpg') ">-->
 <nav class="navbar navbar-default navbar-fixed-top" role="navigation">   
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-left">
<!--                    <img src="img/PixelKit_location_icon.png" alt="" height="70px"/>
                    <h3>School Managment System</h3>-->
                    <!--<li><img src="img/PixelKit_location_icon.png" alt="" height="70px"/></li>-->
                    <li><h2>Name of School</h2><h6>Tel Number</h6></li>
            </ul>
            
            <ul class="nav navbar-nav navbar-right" style="background-color: #EEE;margin-top: 37px">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="" href="#"></a>
                </li>
                <li class="dropdown" >
                    <a  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Setting <span class="caret"></span></a>
                    <ul class="dropdown-menu" >
                        <li><a href="{{route('session_index')}}">View Session</a></li>
                    <li><a href="{{route('config_create')}}">Config</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Student <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('class_index')}}">New Class</a></li>
                    <li><a href="{{route('admission_index')}}">View current student</a></li>
                    <li><a href="{{route('attend_index')}}">Attendance</a></li>
                    <li><a href="{{route('exam_index')}}">Add Exam</a></li>
                    <li><a href="{{route('subject_index')}}">Add Subject</a></li>
                    <li><a href="{{route('marksheet_index')}}">Marksheet</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Time Schedule<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('class_time_index')}}">Class Time</a></li>
                        <li><a href="{{route('priod_index')}}">Set Priod</a></li>
                         <li><a href="{{route('holiday_index')}}">Set Holiday</a></li>
                    </ul>
                </li>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Staff <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('staff_index')}}">Staff</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admission <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('application_index')}}">Application</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fee Managment <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('feehead_create')}}">Add Fee head</a></li>
                        <li><a href="{{route('fee_collection_create')}}">Fee Collection</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

