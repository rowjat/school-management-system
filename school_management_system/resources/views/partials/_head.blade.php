    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="<?php echo csrf_token() ?>">
    <title>SMS_ERP- @yield('title')/{{preg_replace('/([a-z0-9])?([A-Z])/','$1 $2',str_replace('Controller','',explode("@",class_basename(app('request')->route()->getAction()['controller']))[0]))."/".preg_replace('/([a-z0-9])?([A-Z])/','$1 $2',str_replace('Controller','',explode("@",class_basename(app('request')->route()->getAction()['controller']))[1]))}}</title>
    {{Html::style('css/bootstrap.min.css')}}
    {{Html::style('font-awesome-4.7.0/css/font-awesome.min.css')}}
    {{Html::style('css/scrolling-nav.css')}}
    {{Html::style('css/main_style.css')}}
    {{Html::style('notification_msg/animate.css')}}
</head>
