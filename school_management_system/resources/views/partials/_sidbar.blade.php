<div class="nav-side-menu">
    <div class="brand">Current Session Year <?php // echo App\Http\Controllers\setting\SessionController::view_current_session()->name?></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content" style="color: black"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">
                <li>
                  <a href="/">
                  <i class="fa fa-dashboard fa-lg"></i> Dashboard
                  </a>
                </li>

                <li  data-toggle="collapse" data-target="#Session" class="collapsed active">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Setting <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Session">
                    <!--<li class="active"><a href="#">CSS3 Animation</a></li>-->
                    <li><a href="{{route('session_index')}}">View Session</a></li>
                    <li><a href="{{route('config_create')}}">Config</a></li>
                </ul>
                <li  data-toggle="collapse" data-target="#Student" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Student <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Student">
                    <li><a href="{{route('class_index')}}">New Class</a></li>
                    <li><a href="{{route('admission_index')}}">View current student</a></li>
                </ul>
                 <li  data-toggle="collapse" data-target="#Admission" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Admission <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Admission">
                    <li><a href="{{route('application_index')}}">Application</a></li>
                </ul>
                <li  data-toggle="collapse" data-target="#Fee" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Fee Managment <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="Fee">
                    <li><a href="{{route('feehead_create')}}">Add Fee head</a></li>
                </ul>
                 <li>
                  <a href="#">
                  <i class="fa fa-user fa-lg"></i> Profile
                  </a>
                  </li>

                 <li>
                  <a href="#">
                  <i class="fa fa-users fa-lg"></i> Users
                  </a>
                </li>
            </ul>
     </div>
</div>
   