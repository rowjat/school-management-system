<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="badge">Class Time </span>
    </div>
    <div class="panel-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 100px">Day</th>
                    @foreach($priod as $value)
                    <th>{{$value->name}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @php($weekday = [0=>'Satarday',1=>'Sunday',2=>'Monday',3=>'Tuesday',4=>'Wesday',5=>'Thusday',6=>'Friday'])
                @foreach($days as $day)
                <tr>
                    <td rowspan="2"><span class='badge'>{{$weekday[$day->day-1]}}</span></td>
                    @foreach($priod as $value)
                    @php($y = $day->id)
                    @php($z = $value->id)
                    @php($x = $class_time->where('day',$y)->where('priod_id',$z)->first())
                    <td style="background-color: #5bc0de;border-top: 2px solid #EEE;height: 30px">
                        @if($x!=null)
                        {{$x->subject_id}}
                        @endif
                    </td>
                    @endforeach
                </tr>
                <tr>
                    @foreach($priod as $value)
                    @php($y = $day->id)
                    @php($z = $value->id)
                    @php($x = $class_time->where('day',$y)->where('priod_id',$z)->first())
                    <td style="background-color: #B0BEC5;border-bottom: 2px solid #EEE;height: 30px">
                        @if($x!=null)
                        {{$x->staff_id}}
                        @endif
                    </td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

