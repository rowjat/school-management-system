@extends('master')
@section('title','time_schedule')
@section('content')
<div class="page-header">
    <h3>Class Time</h3>
</div>
<div class="row">
    {!!Form::open(['method'=>'POST','route'=>'class_time_store','class'=>'form-horizontal'])!!}
    <div class="col-md-4">
        <div class="row">
            <div class="form-group">
            {!!Form::label('session_id','Session Year :',['class'=>'form-label col-md-4'])!!}
            <div class="col-md-5">
                @php($session_arr = [])
                @php($session_arr[''] = 'select')
                @foreach($session_year as $value)
                @php($session_arr[$value->id] = $value->name)
                @endforeach
                {!!Form::select('session_id',$session_arr,null,['class'=>'form-control'])!!}
            </div>
        </div>
        </div>
        <div class="row">
            <div class="form-group">
            {!!Form::label('class_id','Select Class :',['class'=>'form-label col-md-4'])!!}
            <div class="col-md-5">
                <select name="class_id" id="class_id" class="form-control">
                    <option value="">Select</option>
                </select>
            </div>
        </div>
        </div>
        <div class="row">
        <div class="form-group">
            {!!Form::label('day','Select Day :',['class'=>'form-label col-md-4'])!!}
            <div class="col-md-5">
                {!!Form::select('day',[''=>'Select',1=>'Satarday',2=>'Sunday',3=>'Monday',4=>'Tuesday',5=>'Wednesday',6=>'Thursday',7=>'Friday'],null,['class'=>'form-control'])!!}
            </div>
        </div>
    </div>
        <div class="row">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        
    </div>
    
    <div class="col-md-8">
        <table class="table" style="border: 2px solid #444">
                <thead>
                    <tr>
                        <th style="width: 100px">Period Number</th>
                        <th>Subject</th>
                        <th>Select Staff</th>
                        <th>Period Time</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($priod as $value)
                    <tr>
                        <td>{{$value->name}}<input type="hidden" name="priod_id[]" value="{{$value->id}}"></td>
                        <td>
                            <select name="subject_id[]" id="subject_id" class="form-control subject_id">
                                <option value="">Select</option>
                            </select>
                        </td>
                        <td>
                            @php($staff_arr = ['Select'])
                            @foreach($staff as $v)
                            @php($staff_arr[$v->id] = $v->name)
                            @endforeach
                            {!!Form::select('staff_id[]',$staff_arr,null,['class'=>'form-control'])!!}
                        </td>
                        <td>{{date('H:i A',strtotime($value->from))." - ".date('H:i A',strtotime($value->to))}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
      {!!Form::close()!!}
        
   
</div>

@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */
    $('#session_id').on('change',function(){
        $.ajax({
            url: "class_time_create",
            method: "get",
            data: {
                session_id:$('#session_id').val(),
                i:0
            },
            success: function (data) {
                var result_class = "<option value=''>Select</option>";
                $.each(data.class_time , function(key ,value){
                    result_class += "<option value='"+value.id+"'>"+value.name+"</option>"; 
                });
                $('#class_id').append().empty();
                $('#class_id').append(result_class);
            }
        });
    });       
    $('#class_id').on('change',function(){
        $.ajax({
            url: "class_time_create",
            method: "get",
            data: {
                session_id:$('#session_id').val(),
                class_id:$('#class_id').val(),
                i:1
            },
            success: function (data) {
                var result = "<option value=''>Select</option>";
                $.each(data.subject , function(key ,value){
                    result += "<option value='"+value.id+"'>"+value.name+"</option>"; 
                });
                $('.subject_id').append().empty();
                $('.subject_id').append(result);
            }
        });
    });



            /*
             * End Code
             */
    });
</script>
@endsection

