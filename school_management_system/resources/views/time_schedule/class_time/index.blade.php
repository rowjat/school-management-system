@extends('master')
@section('title','Time_schediule')
@section('content')
<div class="page-header">
    <h3>Class Time Table</h3>
</div>
<div class="row">
    <div class="col-md-4 form-horizontal">
        <div class="form-group ">
            <label for="session_id" class="control-label col-md-6">Session Year</label>
            <div class="col-md-6">
                <select name="session_id" id="session_id" class="form-control">
                    <option value="">Select</option>
                    @foreach($session_year as $value)
                     <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-4 form-horizontal"> 
        <div class="form-group">
            <label for="class_id" class="control-label col-md-6">Class :</label>
            <div class="col-md-6">
                <select name="class_id" id="class_id" class="form-control">
                    
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-4 text-right">
        <p><a href="{{route('class_time_create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Time Schedule</a></p> 
    </div>
</div><br/>
<div class="row" >
    <div class="col-md-10 col-md-offset-1" id="time_table">
        
    </div>
</div>

    
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */

$('#session_id').on('change',function(){
                    $.ajax({
                        url: "class_time_index",
                        method: "get",
                        cache: false,
                        data: {
                            'session_id':$('#session_id').val(),
                            'i':0
                        },
                        success: function (data) {
                            var result = "<option value=''>Select</option>";
                            $.each(data,function(k , v){
                                result +="<option value='"+v.id+"'>"+v.name+"</option>"
                            });
                            $('#time_table').append().empty();
                            $('#class_id').append().empty();
                            $('#class_id').append(result);
                            console.log(data);
                        }
                    });
                });
 $('#class_id').on('change',function(){
                    $.ajax({
                        url: "class_time_index",
                        method: "get",
                        cache: false,
                        data: {
                            'session_id':$('#session_id').val(),
                            'class_id':$('#class_id').val(),
                            'i':1
                        },
                        success: function (data) {
                            $('#time_table').append().empty();
                            $('#time_table').append(data);
                            console.log(data);
                        }
                    });
                });               



            /*
             * End Code
             */
    });
</script>
@endsection
