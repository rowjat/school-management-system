@extends('master')
@section('title','Time_schedule')
@section('content')
<div class="page-header">
    <h2>Priod Page</h2>
</div>
<div class="row">
    <div class="col-md-12">
        <p><a href="#create_subject" class="btn btn-sm btn-primary" data-toggle="modal" data-target=".priod"><i class="fa fa-plus"></i>&nbsp;Set Priod</a></p>
    </div>
</div>
@include('time_schedule.priod.create')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-primary">
        <div class="panel-heading">Priod Time List</div>
        <div class="panel-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>From</th>
                        <th>To</th>
                        <th>To</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($priod as $value)
                    <tr>
                        <td>{{$value->id}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->from}}</td>
                        <td>{{$value->to}}</td>
                        <td><i class="fa fa-eye"></i></td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
                
        </div>
    </div>
    </div>
    
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */





            /*
             * End Code
             */
    });
</script>
@endsection


