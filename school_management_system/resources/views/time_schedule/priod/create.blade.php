<div class="modal fade priod" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridModalLabel">Set Priod</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        {!!Form::open(['method'=>'POST','route'=>'priod_store','class'=>'form-horizontal'])!!}
                        <div class="form-group">
                            {!!Form::label('name','Name :',['class'=>'control-label col-md-4'])!!}
                            <div class="col-md-6">
                                {!!Form::text('name',null,['class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('from','From :',['class'=>'control-label col-md-4'])!!}
                            <div class="col-md-6">
                                {!!Form::time('from',date('00:00'),['class'=>'form-control'])!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('to','To :',['class'=>'control-label col-md-4'])!!}
                            <div class="col-md-6">
                                {!!Form::time('to',date('00:00'),['class'=>'form-control'])!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
