@extends('master')
@section('title','Time_schedule')
@section('content')
<div class="row">
    <div class="col-md-12">
        @php($day = [0=>'Satarday',1=>'Sunday',2=>'Monday',3=>'Tuesday',4=>'Wesday',5=>'Thusday',6=>'Friday'])
        @if($result==true)
            {!! Form::open(['method'=>'PUT','route'=>'holiday_update','class'=>'form-inline']) !!}
            
            @php($i=0)
            @foreach($holiday as $value)
            @php($i++)
            <div class="input-group">
                <label for="{{$i-1}}" class="form-control" >{{$day[$i-1]}}</label>
                <input type="hidden" name="id[{{$i-1}}]" value="{{$value->id}}">
                <input type="hidden" name="day[{{$i-1}}]" value="{{$value->day}}">
                <input type="hidden" id="ststus_{{$i-1}}" name="status[{{$i-1}}]" value="{{$value->status}}">
                <span class="input-group-addon">
                    <input type="checkbox" id="{{$i-1}}" {{$value->status==1?'checked':""}}>
                </span>
            </div>
            @endforeach
        @else
            {!! Form::open(['method'=>'POST','route'=>'holiday_store','class'=>'form-inline']) !!}
             @for($i=0;$i<7;$i++)
            <div class="input-group">
                <label for="{{$i}}" class="form-control" >{{$day[$i]}}</label>
                <input type="hidden" name="day[{{$i}}]" value="{{$i+1}}">
                <input type="hidden" id="ststus_{{$i}}" name="status[{{$i}}]" value=0>
                <span class="input-group-addon">
                    <input type="checkbox" id="{{$i}}">
                </span>
            </div>
            @endfor
        @endif
        
        <div class="input-group">
            <button type="submit" class="btn btn-success">Set Holiday</button>
        </div>
        {!! Form::close() !!} 
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */
$( "input[type=checkbox]" ).on( "change",function(){
    var id = $(this).attr('id');
    if(this.checked){
       $('#ststus_'+id).val(1) 
       console.log(1)
    }else{
        $('#ststus_'+id).val(0) 
        console.log(0);
    }
});
   





            /*
             * End Code
             */
    });
</script>
@endsection
