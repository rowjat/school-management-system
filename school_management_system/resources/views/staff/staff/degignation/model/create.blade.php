<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Designation</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST','route'=>'desig_store','class'=>'form-horizontal']) !!}
                <div class="form-group">
                    <label class="form-label col-md-4" for="name">Name :</label>
                    <div class="col-lg-4">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Designation name" >
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
                {!!Form::close()!!}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

