@extends('master')
@section('title','Staff')
@section('content')
<div class="page-header">
    <h1>Staff</h1>
</div>
@include('staff.staff.degignation.model.create')
<div class="row">
    <form method="post" action="{{route('staff_store')}}" role="form" id="staff" class="form-horizontal"  enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body">
            <div class="well">
                <div class="row">
                    <div class="col-md-6">
                        <span class="badge">Basic Details</span>
                        <div class="form-group">
                            <label for="oname" class="control-label col-md-6">Name :</label>
                            <div class="col-md-6">
                                <input type="text" name="name" id="oname" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_name" class="control-label col-md-6">Fathers Name :</label>
                            <div class="col-md-6">
                                <input type="text" name="f_name" id="f_name" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="m_name" class="control-label col-md-6">Mothers Name :</label>
                            <div class="col-md-6">
                                <input type="text" name="m_name" id="m_name" class="form-control" >
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-6 text-center">   
                        <label for="pictur"><img src="img/PixelKit_location_icon.png" id="preview" height="100" width="100" style="border: 1px solid #269abc"/></label>
                        <input id="pictur" name="pictur" type="file" style="display: none" /><br />
                        <label for="pictur">Choose File</label>

                    </div>
                </div>
            </div>
            <div class="well">
                <div class="row">
                    <div class="col-md-6">
                        <span class="badge">Professional Details</span>
                        <div class="form-group">
                            <label for="dob" class="control-label col-md-6">Date of Birth :</label>
                            <div class="col-md-6">
                                <input type="date" name="dob" id="dob" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nationality" class="control-label col-md-6">Nationality :</label>
                            <div class="col-md-6">
                                <input type="text" name="nationality" id="nationality" class="form-control" >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <br/>
                        <div class="form-group">
                            <label for="gender" class="control-label col-md-4">Gender :</label>
                            <div class="col-sm-3">
                                <input type="radio" name="gender" id="gender" value="m" required="">
                                <label for="sex-m">Male</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6">
                                <input type="radio" name="gender" value="f">
                                <label for="sex-f">Female</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="well">
                        <span class="badge">Contact Details</span>
                        <div class="form-group">
                            <label for="contact_no" class="control-label col-md-6">Contact No :</label>
                            <div class="col-md-6">
                                <input type="text" name="contact_no" id="contact_no" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label col-md-6">E-mail :</label>
                            <div class="col-md-6">
                                <input type="email" name="email" id="email" class="form-control" >
                                <span class="error-msg" id='error_email'></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="control-label col-md-6">Address :</label>
                            <div class="col-md-6">
                                {!!Form::textarea('address',null,['class'=>'form-control','col'=>2,'rows'=>1,'id'=>'address'])!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="well">
                        <span class="badge">Other Details</span>
                        <div class="form-group">
                            <label for="degignation_id" class="control-label col-md-4">Degignation:</label>
                            <div class="col-md-6">
                                <select name="degignation_id" id="degignation_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($desig as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div><span><a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></a></span>

                        </div>
                        <div class="form-group">
                            {!!Form::label('join_date','Join Date :',['class'=>'control-label col-md-4'])!!}
                            <div class="col-md-6">
                                {!!Form::date('join_date',\Carbon\Carbon::now(),['class'=>'form-control'])!!}
                            </div>
                        </div>
                        <br/><br/><br/>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>  
</div>

@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */

$("#pictur").on('change', function () {
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#preview').attr('src',e.target.result);
            }
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    });



            /*
             * End Code
             */
    });
</script>
@endsection

