@extends('master')
@section('title','Staff')
@section('content')
<div class="row">
    <div class="col-md-12 text-right">
        <p> <a href="{{route('staff_create')}}" class="btn btn-sm btn-default">Add Staff</a></p>
    </div>
</div>

<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading"><h4>Staff Details</h4></div>
        <div class="panel-body">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Gander</th>
                        <th>contact_no</th>
                        <th>email</th>
                        <th>degignation_id</th>
                        <th>join_date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($staff as $value)
                    <tr>
                        <td>{{$value->name}}</td>
                        <td>{{$value->gender}}</td>
                        <td>{{$value->contact_no}}</td>
                        <td>{{$value->email}}</td>
                        <td>{{$value->degignation_id}}</td>
                        <td>{{$value->join_date}}</td>
                        <td><a href="#"><i class="fa fa-eye-slash"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    @include('partials._msg')
            /*
             * Start code
             */


    
            /*
             * End Code
             */
    });
</script>
@endsection

