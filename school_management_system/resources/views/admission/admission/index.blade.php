@extends('master')
@section('title','Admission')
@section('content')
<!-- Modal -->
<div class="modal fade" id="student" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #B0BEC5">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Student Details</h4>
            </div>
            <div class="modal-body" id="result">

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!---------------->
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>View Current session Student</h3></div>

            <table class="table table-sm" style="">
                <thead>
                    <tr>
                        <th>Admission no</th>
                        <th>Name Of Student</th>
                        <th>Admission Date</th>
                        <th>Class</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($admissions as $admission) 
                    <tr>
                        <td>{{$admission->id}}</td>
                        <td>{{$admission->student_id}}</td>
                        <td>{{$admission->date}}</td>
                        <th>{{$admission->school_class_id}}</th>
                        <th><a href="#" class="student" data-toggle="modal" id="{{$admission->id}}" data-target="#student"><i class="fa fa-eye"></i></a></th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$admissions->render()}}

        </div>
    </div>
</div> 
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */

             $(document).on('click','.student',function(){
                 var id = $(this).attr('id');
                 $.ajax({
                    url: "student_view",
                    type: "GET",
                    async: false,
                    data: {
                        'id':id,
                        'i':'student'
                    },
                    success: function (data) {
                        $("#result").append().empty();
                         $("#result").append(data);
                        }
                        
                });
                 console.log(id);
             });
             $(document).on('click','.attend',function(){
                 var id = $(this).attr('id');
                 $.ajax({
                    url: "student_view",
                    type: "GET",
                    async: false,
                    data: {
                        'id':id,
                        'i':'attend'
                    },
                    success: function (data) {
                        $("#result").append().empty();
                         $("#result").append(data);
                        }
                        
                });
                 console.log(id);
             });
             $(document).on('click','.exam',function(){
                 var id = $(this).attr('id');
                 $.ajax({
                    url: "student_view",
                    type: "GET",
                    async: false,
                    data: {
                        'id':id,
                        'i':'exam'
                    },
                    success: function (data) {
                        $("#result").append().empty();
                         $("#result").append(data);
                        }
                        
                });
                 console.log(id);
             });
             $(document).on('click','.fee',function(){
                 var id = $(this).attr('id');
                 $.ajax({
                    url: "student_view",
                    type: "GET",
                    async: false,
                    data: {
                        'id':id,
                        'i':'fee'
                    },
                    success: function (data) {
                        $("#result").append().empty();
                         $("#result").append(data);
                        }
                        
                });
                 console.log(id);
             });
             $(document).on('change','.exam_id',function(){
                 var id = $(this).val();
                    admission_id = $(this).attr('id');
                 $.ajax({
                    url: "student_view",
                    type: "GET",
                    async: false,
                    data: {
                        'id':id,
                        'admission_id':admission_id,
                        'i':'exam_id'
                    },
                    success: function (data) {
                        $("#exam_result").append().empty();
                         $("#exam_result").append(data);
                        }
                        
                });
                 console.log(admission_id);
             });

            /*
             * End Code
             */
    });
</script>
@endsection



