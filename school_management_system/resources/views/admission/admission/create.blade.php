@extends('master')
@section('title','Admission')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>View All Session Year</h2></div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','route'=>'admission_store','class'=>'form-horizontal']) !!}
                <div class="form-group">
                    <label for="student_id" class="col-sm-2 control-label">Reg No :</label>
                    <label class="col-sm-3 control-label" style="color: #2e6da4;text-align: left"><?php echo Session::has('reg') ? Session::get('reg') : "" ?></label>
                    <div class="col-sm-3">

                        @if(Session::has('reg'))
                        {!! Form::hidden('student_id',Session::get('reg'),['class'=>'form-control','id'=>'application_create_1']) !!}
                        @endif
                    </div>
                    <label for="date" class="col-sm-6 control-label">Admission Date :<span style="color: #2e6da4">{{\Carbon\Carbon::now()->format('d-m-Y')}}</span></label>
                    <div class="col-sm-3">
                        <span></span>
                        {!! Form::hidden('date',\Carbon\Carbon::now(),['class'=>'form-control','data-parsley-required'=>'true']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="session_year_id" class="col-sm-2 control-label">Session Year :</label>
                    <label class="col-sm-3 control-label" style="color: #2e6da4;text-align: left">{{App\Http\Controllers\setting\SessionController::view_current_session()['name']}}</label>
                    <div class="col-sm-3">
                        {!! Form::hidden('session_year_id',App\Http\Controllers\setting\SessionController::view_current_session()['id'],null,['class'=>'form-control','id'=>'application_create_1']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="school_class_id" class="col-sm-2 control-label">Class Name :</label>
                    <label class="col-sm-3 control-label" style="color: #2e6da4;text-align: left"><?php echo Session::has('class_id') ? Session::get('class_id') : "" ?></label>
                    <div class="col-sm-3">
                        @if(Session::has('class_id'))
                        {!! Form::hidden('school_class_id',Session::get('class_id'),['class'=>'form-control','id'=>'application_create_1']) !!}
                        @endif
                    </div>
                </div>
                <hr/>
                <div class="form-group" style="background-color: #eee;font-weight: bold">
                    <div class="col-md-1">#</div>
                    <div class="col-md-5">Feehead</div>
                    <div class="col-md-3">Amount</div>
                    <div class="col-md-3">Total</div>
                </div>
                <?php
                $i = 0;
                $total = 0;
                ?>
                @if(Session::has('fees'))
                @foreach(Session::get('fees') as $value)
                <?php
                $i++;
                $total += $value->amount;
                ?>
                <div class="form-group" >
                    <div class="col-md-1"><input type="checkbox" id='fee_{{$i}}' name="feehead_id[]" value="{{$value->id}}"></div>
                    <div class="col-md-5" >{{$value->name}}</div>
                    <div class="col-md-3" style="color: #aaa" id='x_fee_{{$i}}'>{{$value->amount}}</div>
                    <div class="col-md-3" id='y_fee_{{$i}}'>00</div><input type="text" id='amount_fee_{{$i}}' name="amount[]" value="">
                    <!--<div class="col-md-3" style='text-align: right' id='y_fee_2'>0</div><input type="text" id='amount_fee_2' name="amount[2]" value="">-->
                </div>
                @endforeach
                @endif
                <!------------->
                <!--        <div class="form-group" >
                            <div class="col-md-1"><input type="checkbox"  id='fee_1' name="feehead_id[1]" value="1"></div>
                            <div class="col-md-5" >tuion fee</div>
                            <div class="col-md-3" style="color: #aaa;text-align: right" id='x_fee_1'>100</div>
                            <div class="col-md-3" id='y_fee_1' style="text-align: right">0</div><input type="text" id='amount_fee_1' name="amount[1]" value="">
                        </div>
                        <div class="form-group" >
                            <div class="col-md-1"><input type="checkbox"  id='fee_2' name="feehead_id[2]" value="1"></div>
                            <div class="col-md-5" >tuion fee</div>
                            <div class="col-md-3" style="color: #aaa;text-align: right" id='x_fee_2'>200</div>
                            <div class="col-md-3" style='text-align: right' id='y_fee_2'>0</div><input type="text" id='amount_fee_2' name="amount[2]" value="">
                        </div>-->
                <!----------------->
                <hr/>
                <div class="form-group" style="font-weight: bold">
                    <div class="col-md-6">total</div>
                    <div class="col-md-3" style="color: #eee;text-align: right">{{$total}}</div>
                    <div class="col-md-3" id='total' style="text-align: right">0</div>
                </div><hr/>
                <div class="form-group" style="width: ">
                    <div class="col-sm-offset-9 col-sm-3 ">
                        {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                        <a href="{{route('admission_index')}}" class="btn btn-danger">Cancle</a>
                    </div>
                </div>
                <hr/>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
                var total =0;
       $('input:checkbox').on('change', function() {
            
                var fee_id = $(this).attr('id');
                    rechivable_selector ='#x_'+fee_id;
                    amount_selector     = '#y_'+fee_id;
                    amount_input        = '#amount_'+fee_id;
                    console.log(amount_input);
                    rechivable_amount   = $(rechivable_selector).text();
                    if(this.checked) {
                    setamount              = $(amount_selector).text(rechivable_amount);
                   setinput               = $(amount_input).attr('value',rechivable_amount);
                    amount =Number($(amount_selector).text());
                    total +=amount;
                      console.log(total);
                     $('#total').text(total);
            }else{
                
                    amount =Number($(amount_selector).text());
                     setinput               = $(amount_input).attr('value',0);
                    setamount              = $(amount_selector).text(0);
                    
                    total -=amount;
                      console.log(total);
                     $('#total').text(total);
            }
            });
       
       /*
        * End Code
        */
    });
    </script>
@endsection


