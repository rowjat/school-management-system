<div class="row">
    <div class="col-md-11 col-md-offset-1">
        <button class="btn btn-default student" id="{{$student->id}}">Student Details</button>
        <button class="btn btn-default attend" id="{{$student->id}}">Attendance</button>
        <button class="btn btn-default exam" id="{{$student->id}}">Examination </button>
        <button class="btn btn-default fee" id="{{$student->id}}">Fee </button>
    </div>
</div><hr/>
<div class="row">
    <div class="col-md-4">
        <div class="well">
            <div class="row ">
                <div class="col-md-offset-1 col-md-4 ">
                    {{Html::image("uploads/".$student->student->pictur,$student->student->name,['height'=>'120','class'=>'thumb'])}}
                </div>
            </div><hr/>
            <div class="row">
                <div class="col-md-12">
                    <p><strong>Name :</strong> {{$student->student->name}}</p>
                    <p><strong>Class :</strong> {{$student->school_class_id}}</p>
                    <?php
                    $date = new DateTime($student->student->birth_date);
                    $now = new DateTime();
                    $interval = $now->diff($date);
                    ?>
                </div>
            </div>
        </div>
    </div> 
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">Student Profile</div>
            <div class="panel-body" style="overflow: auto;max-height: 300px">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th style="width: 200px">Date Of Birth :</th>
                        <td>{{$student->student->birth_date}}</td>
                    </tr>
                    <tr>
                        <th>Age :</th>
                        <td><span class='badge'>{{$interval->y}}</span> Years <span class='badge'>{{$interval->m}}</span> Month <span class='badge'>{{$interval->d}}</span> Day</td>
                    </tr>
                    <tr>
                        <th>Gender :</th>
                        <td>{{$student->student->gender=='m'?'Male':'Female'}}</td>
                    </tr>
                    <tr>
                        <th>Father Name :</th>
                        <td>{{$student->student->f_name}}</td>
                    </tr>
                    <tr>
                        <th>Mother Name :</th>
                        <td>{{$student->student->m_name}}</td>
                    </tr>
                    <tr>
                        <th>E-mail :</th>
                        <td>{{$student->student->email}}</td>
                    </tr>
                    <tr>
                        <th>Religion :</th>
                        <td>{{$student->student->religion}}</td>
                    </tr>
                    <tr>
                        <th>Nationality :</th>
                        <td>{{$student->student->nationality}}</td>
                    </tr>
                    <tr>
                        <th>Admitted Class :</th>
                        <td>{{$student->student->school_class_id}}</td>
                    </tr>
                    <tr>
                        <th>Date of admission :</th>
                        <td>{{$student->student->date}}</td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>