<div class="row">
    <div class="col-md-11 col-md-offset-1">
        <button class="btn btn-default student" id="{{$student->id}}">Student Details</button>
        <button class="btn btn-default attend" id="{{$student->id}}">Attendance</button>
        <button class="btn btn-default exam" id="{{$student->id}}">Examination </button>
        <button class="btn btn-default fee" id="{{$student->id}}">Fee </button>
    </div>
</div><hr/>
<div class="row">
    <div class="col-md-4">
        <div class="well">
            <div class="row ">
                <div class="col-md-offset-1 col-md-4 ">
                    {{Html::image("uploads/".$student->student->pictur,$student->student->name,['height'=>'120','class'=>'thumb'])}}
                </div>
            </div><hr/>
            <div class="row">
                <div class="col-md-12">
                    <p><strong>Name :</strong> {{$student->student->name}}</p>
                    <p><strong>Class :</strong> {{$student->school_class_id}}</p>
                </div>
            </div>
        </div>
    </div> 
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">Fee</div>
            <div class="panel-body" style="overflow: auto;max-height: 300px">
                <table class="table table-bordered" >
                    <tr>
                        <th colspan="2">Feehead</th>
                        <th>Fee</th>
                        <th>Paid</th>
                        <th>Balance</th>
                    </tr>
                    @foreach($feehead as $value)
                    @if($value->collection_type==1)
                    <tr style="font-size: 10px;">
                        <th rowspan="14" style="font-size: 15px;">{{$value->name}}</th>
                        <th style="background-color: #c4e3f3">Month</th>
                        <th style="background-color: #c4e3f3">Monthly Fee</th>
                        <th style="background-color: #c4e3f3">Monthly bill pay</th>
                        <th style="background-color: #c4e3f3">Balance</th>
                    </tr>
                    @php($t=0)
                    @for($i=0;$i<12;$i++)
                    @php($fee =$fee_collection->where('fee_head_id',$value->id)->where('month',(int)date('m', strtotime("+" . $i . " months", strtotime($month))))->first())
                    @php($pay = $fee==null?0:$fee->amount)
                    @php($t+=$pay)
                    <tr style="font-size: 10px;">
                        <th>{{date('M-Y', strtotime("+" . $i . " months", strtotime($month)))}}</th>
                        <th class="amount">{{round($value->amount/12)}}</th>
                        <th class="amount">{{$pay}}</th>
                        <th class="amount">{{round($value->amount/12)-$pay}}</th>
                    </tr>
                    @endfor
                    <tr style="font-size: 12px">
                        <th>Total</th>
                        <th class="amount">{{round($value->amount/12)*12}}</th>
                        <th class="amount">{{$t}}</th>
                        <th class="amount">{{round($value->amount/12)*12 -$t}}</th>
                    </tr>
                    @else
                    @php($fee =$fee_collection->where('fee_head_id',$value->id)->first())
                    @php($pay = $fee==null?0:$fee->amount)
                    <tr >
                        <th colspan="2">Libiry</th>
                        <th class="amount">{{$value->amount}}</th>
                        <th class="amount">{{$pay}}</th>
                        <th class="amount">{{$value->amount-$pay}}</th>
                    </tr>
                    @endif
                    @endforeach
                </table>
            </div>
        </div>

    </div>
</div>


