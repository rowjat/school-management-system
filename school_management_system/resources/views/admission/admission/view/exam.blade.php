<div class="row">
    <div class="col-md-11 col-md-offset-1">
        <button class="btn btn-default student" id="{{$student->id}}">Student Details</button>
        <button class="btn btn-default attend" id="{{$student->id}}">Attendance</button>
        <button class="btn btn-default exam" id="{{$student->id}}">Examination </button>
        <button class="btn btn-default fee" id="{{$student->id}}">Fee </button>
    </div>
</div><hr/>
<div class="row">
    <div class="col-md-4">
        <div class="well">
            <div class="row ">
                <div class="col-md-offset-1 col-md-4 ">
                    {{Html::image("uploads/".$student->student->pictur,$student->student->name,['height'=>'120','class'=>'thumb'])}}
                </div>
            </div><hr/>
            <div class="row">
                <div class="col-md-12">
                    <p><strong>Name :</strong> {{$student->student->name}}</p>
                    <p><strong>Class :</strong> {{$student->school_class_id}}</p>
                </div>
            </div>
        </div>
    </div> 
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-9">
                        Exam
                    </div>
                    <div class="col-md-3 form-group-sm">
                        <select class="form-control exam_id" id="{{$student->id}}">
                            <option>Select</option>
                            @foreach($exam as $value)
                            <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
            <div class="panel-body" style="overflow: auto;height: 290px" id="exam_result">
                <h6 style='color: #bababa;font-size: 80px'>Select Exam</h6>

            </div>
        </div>

    </div>
</div>
