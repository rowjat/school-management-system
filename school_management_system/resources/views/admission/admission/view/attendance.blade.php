<div class="row">
    <div class="col-md-11 col-md-offset-1">
        <button class="btn btn-default student" id="{{$student->id}}">Student Details</button>
        <button class="btn btn-default attend" id="{{$student->id}}">Attendance</button>
        <button class="btn btn-default exam" id="{{$student->id}}">Examination </button>
        <button class="btn btn-default fee" id="{{$student->id}}">Fee </button>
    </div>
</div><hr/>
<div class="row">
    <div class="col-md-4">
        <div class="well">
            <div class="row ">
                <div class="col-md-offset-1 col-md-4 ">
                    {{Html::image("uploads/".$student->student->pictur,$student->student->name,['height'=>'120','class'=>'thumb'])}}
                </div>
            </div><hr/>
            <div class="row">
                <div class="col-md-12">
                    <p><strong>Name :</strong> {{$student->student->name}}</p>
                    <p><strong>Class :</strong> {{$student->school_class_id}}</p>
                </div>
            </div>
        </div>
    </div> 
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">Attendance</div>
            <div class="panel-body" style="overflow: auto;max-height: 300px">
                <table class="table table-bordered">
                    <tr>
                        <th>Name of Month</th>
                        <th>Academic Day</th>
                        <th>P</th>
                        <th>A</th>
                    </tr>
                    @php($t=0)
                    @php($tp=0)
                    @php($ta=0)
                    @for($i=0;$i<=12;$i++)
                    <?php
                    $start = new \DateTime(date('M-Y-1', strtotime("+" . $i . " months", strtotime($month))));
                    $start->modify('first day of this month');
                    $end = new \DateTime(date('M-Y-1', strtotime("+" . $i . " months", strtotime($month))));
                    $end->modify('last day of this month');

                    $a = \App\student\Attendance::where('session_id', App\Http\Controllers\setting\SessionController::view_current_session()['id'])
                            ->where('admission_id', $student->id)
                            ->whereBetween('date', [$start, $end])
                            ->where('attand_cond', 'A')
                            ->count();
                    $p = \App\student\Attendance::where('session_id', App\Http\Controllers\setting\SessionController::view_current_session()['id'])
                            ->where('admission_id', $student->id)
                            ->whereBetween('date', [$start, $end])
                            ->where('attand_cond', 'P')
                            ->count();
                    $t += $p + $a;
                    $tp += $p;
                    $ta += $a
                    ?>
                    <tr>
                        <th>{{date('M-Y', strtotime("+" . $i . " months", strtotime($month)))}}</th>
                        <td>{{$p+$a}}</td>
                        <td>{{$p}}</td>
                        <td>{{$a}}</td>
                    </tr>
                    @endfor
                    <tr>
                        <th>Total</th>
                        <th>{{$t}}</th>
                        <th>{{$tp}}</th>
                        <th>{{$ta}}</th>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div> 


