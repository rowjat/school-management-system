@extends('master')
@section('title','Admission')
@section('content')
<div class="row">
    <div class="col-md-12 text-right">
        @if(isset($data))
        <a href="{{route('admission_pdf',['data'=>$data['admission']])}}">Pdf</a>
        @endif
    </div>
</div><hr/>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Admission Details</h3></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-0 text-right">
                            <strong>Admission No :</strong>
                        </div>
                        <div class="col-md-6 text-left">
                            {{$data['admission']}}
                        </div> 
                    </div> 
                    <div class="row">
                        <div class="col-md-6 col-md-offset-0 text-right">
                            <strong>Nmae :</strong>
                        </div>
                        <div class="col-md-6 text-left">
                            {{$data['admission']}}
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-0 text-right">
                            <strong>Session Year :</strong>
                        </div>
                        <div class="col-md-6 text-left">
                            {{$data['session_id']}}
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-0 text-right">
                            <strong>Name Of Class :</strong>
                        </div>
                        <div class="col-md-6 text-left">
                            {{$data['class_id']}}
                        </div> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6 text-right">
                        <strong> Admission Date:</strong>
                    </div>
                    <div class="col-md-6 text-left">
                        {{$data['date']}}
                    </div>
                </div>


            </div><hr/>
            <div class="row" >
                <div class="col-md-10 col-md-offset-1" style="background-color: #67b168">
                    <div class="col-md-1 text-left">
                        <p>#</p>
                    </div>
                    <div class="col-md-8 text-left">
                        <p>Feehead</p>
                    </div>
                    <div class="col-md-3 text-left">
                        <p>Amount</p>
                    </div>
                </div>
            </div>
            <?php
            $sr = 0;
            $total = 0;
            foreach ($data['fee'] as $value) {
                $sr++;
                $total += $value->amount;
                ?>
                <div class="row" >
                    <div class="col-md-10 col-md-offset-1">
                        <div class="col-md-1 text-left">
                            <p>{{$sr}}</p>
                        </div>
                        <div class="col-md-8 text-left">
                            <p>{{$value->feehead_id}}</p>
                        </div>
                        <div class="col-md-3 text-left">
                            <p>{{$value->amount}}</p>
                        </div>
                    </div>
                </div>
            <?php } ?>  
            <div class="row" >
                <div class="col-md-10 col-md-offset-1" style="border: 1px solid black;font-weight: bold">
                    <div class="col-md-9 text-left">
                        <p>Total</p>
                    </div>
                    <div class="col-md-3 text-left">
                        <p>{{$total}}</p>
                    </div>
                </div>
            </div><hr/>

        </div>
    </div>
</div> 
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */

            /*
             * End Code
             */
    });
</script>
@endsection

