@extends('master')
@section('title','Admission')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Admission Entry Form</h2></div>
            <div class="panel-body">
                Basic Details<hr/>
                {!! Form::open(['method'=>'POST','files'=>'true','route'=>'student_store','class'=>'form-horizontal']) !!}

                @if(Session::has('id'))
                <input type="hidden" name="application_id" value="{{Session::get('id')}}">
                @endif
                <div class="form-group">
                    {!! Form::label('name','Name :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('f_name','Father Name :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('f_name',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('m_name','Mother Name :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('m_name',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('birth_date','Date of birth :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::date('birth_date',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('gender','Gender :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-3">
                        <input type="radio" name="gender" value="m">
                        <label for="sex-m">Male</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4"></div>  
                    <div class="col-sm-3">
                        <input type="radio" name="gender" value="f">
                        <label for="sex-f">Female</label>
                    </div>
                </div>
                Academic Details<hr/>
                <div class="form-group">
                    {!! Form::label('date','Date :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::date('date',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('school_class_id','Class :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        <?php
                        $class_arr = '';
                        if (Session::has('school_classes')) {
                            $class_arr = Session::get('school_classes');
                        }
                        ?>
                        {!! Form::text('school_class_id',$class_arr,['class'=>'form-control']) !!}
                    </div>
                </div>
                Contact Details<hr/>
                <div class="form-group">
                    {!! Form::label('contact_no','Contact No :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('contact_no',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('email','E-mail :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::email('email',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('address','Address :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::textarea('address',null,['class'=>'form-control','style'=>'height:50px']) !!}
                    </div>
                </div>
                Other Details<hr/>
                <div class="form-group">
                    {!! Form::label('birth_place','Birth Place :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('birth_place',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('nationality','Nationality :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('nationality',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('religion','Religion :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('religion',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('pictur','Pictur :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        <label class="btn btn-default btn-file">
                            Browse <input type="file" name="pictur" style="display: none;">
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-10 ">
                        {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                        <a href="{{route('application_index')}}" class="btn btn-danger">Cancle</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
        
        
        
        
        
       /*
        * End Code
        */
    });
    </script>
@endsection


