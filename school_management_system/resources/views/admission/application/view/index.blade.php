@foreach($apps as $app) 
<tr>
    <td>{{$app->name}}</td>
    <td>{{$app->contact}}</td>
    <td>{{$app->email}}</td>
    <td>{{$app->date}}</td>
    <td>{{$app->session_year_id}}</td>
    <td>{{$app->school_class_id}}</td>
    <td>{{$app->amount}}</td>
    <td>
        @if($app->select == "")
        {!! Form::open(['method'=>'PUT','route'=>'application_update','class'=>'form-horizontal']) !!}
        <input type="hidden" name="id" value="{{$app->id}}">
        <input type="hidden" name="select" value="selected">
        <button type="submit" class="btn btn-sm btn-default">For Select</button>
        {!! Form::close()!!}
        @elseif($app->select == "selected")
        {!! Form::open(['method'=>'PUT','route'=>'application_update','class'=>'form-horizontal']) !!}
        <input type="hidden" name="id" value="{{$app->id}}">
        <input type="hidden" name="select" value="admitted">
        <button type="submit" class="btn btn-sm btn-success">For Admission</button>
        {!! Form::close()!!}
        @else
        admitted
        @endif
    </td>
    <td><a href="#" class="btn btn-sm btn-default">Edit</a></td>
</tr>
@endforeach

