@extends('master')
@section('title','Admission')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Application Entry form</h2></div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','route'=>'application_store','class'=>'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('name','Name :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('name',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('contact','Contact No :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('contact',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('email','Email No :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::email('email',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('date','Date :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::date('date',\Carbon\Carbon::now(),['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('session_year_id','Session Year :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        <?php
                        $year_arr = ['Select'];
                        foreach ($session_years as $session_year) {
                            $year_arr[$session_year->id] = $session_year->name;
                        }
                        ?>
                        {!! Form::select('session_year_id',$year_arr,null,['class'=>'form-control','id'=>'application_create_1']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('school_class_id','For class :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        <select name="school_class_id" id='school_class_id' class="form-control">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('amount','Amount :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::number('amount',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                    </div>
                </div>
                <div class="form-group" style="width: ">
                    <div class="col-sm-offset-4 col-sm-10 ">
                        {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                        <a href="{{route('application_index')}}" class="btn btn-danger">Cancle</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
         $('#application_create_1').on('change', function () {
                $.ajax({
                    url: "application_create_1",
                    type: "GET",
                    async: false,
                    data: {'id':$("#application_create_1").val()},
                    success: function (data) {
                        console.log(data);
                        $('#school_class_id').append(" <option value=''></option>");
                        if(data){
                        var option = ""; 
                        $.each(data,function(index,obj){
                             option += " <option value='"+obj.id+"'>"+obj.name+"</option>";  
                        });
                        $('#school_class_id').append(option).empty();
                        $('#school_class_id').append(option);
                        }else{
                            $('#school_class_id').append(option).empty();
                        }
                    }    
                });
            });
        
       /*
        * End Code
        */
    });
    </script>
@endsection

