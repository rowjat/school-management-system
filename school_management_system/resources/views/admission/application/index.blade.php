@extends('master')
@section('title','Admission')
@section('content')
<div class="row">
    <div class="col-md-12">
        <a href="{{route('application_create')}}" class="btn btn-success">Application Sale</a>
    </div>
</div><hr/>
<div class="row">
    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>View Current Year Sale Application</h2></div>
            <div class="panel-body">
                <table class="table table-sm table-hover tbl-main">
                    <thead>
                        <tr>
                            <th>name</th>
                            <th>contact</th>
                            <th>email</th>
                            <th>date</th>
                            <th>session_year_id</th>
                            <th>school_class_id</th>
                            <th>amount</th>
                            <th>select</th>
                            <th></th>

                        </tr>
                        <tr>
                            <td><input type="text" class="form-control" style="height: 20px" value="" name="name" id="name"></td>
                            <td><input type="text" class="form-control" style="height: 20px" name="contact" id="contact"></td>
                            <td><input type="text" class="form-control" style="height: 20px" name="email" id="email"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody id="showme">
                        @if(isset($apps))
                        @foreach($apps as $app) 
                        <tr>
                            <td>{{$app->name}}</td>
                            <td>{{$app->contact}}</td>
                            <td>{{$app->email}}</td>
                            <td>{{$app->date}}</td>
                            <td>{{$app->session_year_id}}</td>
                            <td>{{$app->school_class_id}}</td>
                            <td>{{$app->amount}}</td>
                            <td>
                                @if($app->select == "")
                                {!! Form::open(['method'=>'PUT','route'=>'application_update','class'=>'form-horizontal']) !!}
                                <input type="hidden" name="id" value="{{$app->id}}">
                                <input type="hidden" name="select" value="selected">
                                <button type="submit" class="btn btn-sm btn-default">For Select</button>
                                {!! Form::close()!!}
                                @elseif($app->select == "selected")
                                {!! Form::open(['method'=>'PUT','route'=>'application_update','class'=>'form-horizontal']) !!}
                                <input type="hidden" name="id" value="{{$app->id}}">
                                <input type="hidden" name="select" value="admitted">
                                <button type="submit" class="btn btn-sm btn-success">For Admission</button>
                                {!! Form::close()!!}
                                @else
                                admitted
                                @endif
                            </td>
                            <td><a href="{{route('application_edit',$app->id)}}" class="btn btn-sm btn-default">Edit</a></td>

                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                @if(isset($apps))
                {{$apps->render()}}
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
       
        $('#name').keyup(function () {
            var tbl_name = $(this).attr('name');
                valu = $(this).val();
             data(tbl_name , valu);
               
            }); 
        $('#contact').keyup(function () {
        var tbl_name = $(this).attr('name');
            valu = $(this).val();
         data(tbl_name , valu);

        });
        
        $('#email').keyup(function () {
        var tbl_name = $(this).attr('name');
            valu = $(this).val();
         data(tbl_name , valu);

        });
        $('#school_class_id').keyup(function () {
        var tbl_name = $(this).attr('name');
            valu = $(this).val();
         data(tbl_name , valu);

        });
        $('#selected').keyup(function () {
        var tbl_name = $(this).attr('name');
            valu = $(this).val();
         data(tbl_name , valu);

        });
       
       function data(tbl_name , valu){
            $.ajax({
                    url: "application_index",
                    type: "GET",
                    async: false,
                    data: {
                        'tbl_name':tbl_name,
                        'valu':valu
                    },
                    success: function (data) {
                        console.log(data);
                         $('#showme').append().empty();
                         $('#showme').append(data);
                        }
                        
                });
       }
         
        
        
        
        
       /*
        * End Code
        */
    });
    </script>
@endsection

