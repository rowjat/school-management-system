@extends('master')
@section('title','Student')
@section('content')
<div class="row">
    <div class="col-md-12 text-right">
        

        <a href="class_create" class="btn btn-primary">Create new Class</a>
    </div>
</div><hr/>
<div class="row">
    <div class="col col-lg-8" id="showme">
   
    </div>
    <div class="row">
     <div class="col col-md-4 ">
        {!! Form::open(['class'=>'form-horizontal','id'=>'form']) !!}
        <div class="form-group">
            {!! Form::label('session_years_id','Select Session :',['class'=>'col-sm-5 control-label']) !!}
            <div class="col-sm-7">
                <?php
                $session_years_arr = ['Select'];
                    foreach ($session_years as $session_year) {
                        $session_years_arr[$session_year->id] = $session_year->name;
                    }
                ?>

                {!! Form::select('session_years_id',$session_years_arr,null,['class'=>'form-control','id'=>'ajax_class_index'])!!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
        $('#ajax_class_index').on('change', function () {
                $.ajax({
                    url: "ajax_class_index",
                    type: "GET",
                    async: false,
                    data: $("#form").serialize(),
                    success: function (data) {
//                        console.log(data);
//                        
                         $("#showme").html(data);
                        }
                        
                });
            });
            $(document).on('click','.pagination a', function (e) {
                        e.preventDefault();
                        var page = $(this).attr('href').split('page=')[1]; 
                $.ajax({
                    url: "ajax_class_index_page?page="+page,
                    type: "GET",
                    async: false,
                    data: {'id':$('#ajax_class_index').val()},
                    success: function (data) {
                        console.log(data);
//                        
                         $("#showme").html(data);
                        } 
                });
            });
        
        
        
        
       /*
        * End Code
        */
    });
    </script>
@endsection


           
                
                    
                    
                    
                
            
            
            
       