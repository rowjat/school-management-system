<div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>View All Class</h3></div>

            <table class="table table-hover">
    <thead style=" background-color: #398439">
        <tr>
            <th>#</th>
            <th>id</th>
            <th>name</th>
            <th class="tbl-col-1"></th>
        </tr>
    </thead>
    <tbody>
       @foreach($data as $dat) 
        <tr>
            <th scope="row">1</th>
            <td>{{$dat->id}}</td>
            <td>{{$dat->name}}</td>
            <td><a href="#" class="btn btn-sm btn-default">Edit</a></td>
        </tr>
    @endforeach
    </tbody>
</table>    
        </div>
        {{$data->render()}}
</div> 

