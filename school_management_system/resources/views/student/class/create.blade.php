@extends('master')
@section('title','Student')
@section('content')
<div class="row">
    <div class="col-md-12">
        <button class="btn btn-default">Create new Session Year</button>
    </div>
</div><hr/>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>New Class Entry Form</h2></div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','route'=>'class_store','class'=>'form-horizontal']) !!}
                    <div class="form-group">
                    {!! Form::label('session_years_id','Select Session :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        <?php
                    $session_arr = [];
                    foreach ($session_year as $sess) {
                        $session_arr[$sess->id] = $sess->name;
                    }
                    ?>
                        
                    {!! Form::select('session_years_id',$session_arr,null,['class'=>'form-control'])!!}
                    </div>
                    </div>
                    <div class="form-group">
                    {!! Form::label('name','Name Of Class :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="form-group" style="width: ">
    <div class="col-sm-offset-4 col-sm-10 ">
                    {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                    <a href="{{route('class_index')}}" class="btn btn-danger">Cancle</a>
    </div>
                     </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
        
        
        
        
        
       /*
        * End Code
        */
    });
    </script>
@endsection

