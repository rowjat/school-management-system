@extends('master')
@section('title','Student')
@section('content')
<hr/>
<div class="row">
    {!! Form::open(['method'=>'PUT','route'=>'attend_update','class'=>'form-horizontal']) !!}
    
    <div class="col-md-4">
        <div class="well">
            <div class="page-header">
  <h1>Student Attendance</h1>
</div>
        <table>
            <tr>
                <th>Session Year :</th>
                <td><span class='badge d-attend-cond'> {{$get_session->name}}<input type="hidden" name="session_id" value="{{$get_session->id}}"></span></td>
            </tr>
            <tr>
                <th>Class Name :</th>
                <td><span class="badge d-attend-cond">{{$get_class_id->name}}<input type="hidden" name="class_id" value="{{$get_class_id->id}}"></span></td>
            </tr>
            <tr>
                <th>Date :</th>
                <td><span class="badge d-attend-cond">{{$date}}<input type="hidden" name="date" value="{{$date}}"></span></td>
            </tr>
        </table>
        </div>
        <div class="col-sm-12">
                {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                <a href="{{route('admission_index')}}" class="btn btn-danger">Cancle</a>
            </div>
    </div>
    <div class="col-md-8">
        <div class="well">
            <div class="row text-right">
                <div class="col-md-12">
                    <table>
                <tr>
                    @if($student->where('attand_cond','H')->count()>0)
                    <th><input type="checkbox" checked="" id="holiday" >&nbsp;&nbsp;<span class="badge">&nbsp;&nbsp;&nbsp;Holiday</span></th>
                    @else
                    <th><input type="checkbox" id="holiday" >&nbsp;&nbsp;<span class="badge">&nbsp;&nbsp;&nbsp;Holiday</span></th>
                    @endif
                </tr>
                <tr>
                    @if($student->where('attand_cond','H')->count()>0)
                    <th><input type="checkbox" disabled="" id="selectall" class="single">&nbsp;&nbsp;<span class="badge">All Select</span></th>
                    @else
                    <th><input type="checkbox" id="selectall" class="single">&nbsp;&nbsp;<span class="badge">All Select</span></th>
                    @endif
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th><span class='badge p-attend-cond'>Present</span></th>
                    <td><span class='badge p-attend-cond' id='p'>{{$student->where('attand_cond','P')->count()}}</span>&nbsp;&nbsp;</td>
                    <th><span class='badge a-attend-cond'>Absent</span></th>
                    <td><span class="badge a-attend-cond" id='a'>{{$student->where('attand_cond','A')->count()}}</span>&nbsp;&nbsp;</td>
                    <th><span class='badge d-attend-cond'>Total</span></th>
                    <td><span class="badge d-attend-cond">{{$student->count()}}</span></td>
                </tr>
            </table>  
                </div>
            </div><br/>
        <div style="overflow: auto;max-height: 400px;border: 2px solid #ccc">
            @php( $i =0)
            @foreach($student as $value)
            @php($i++)
            <div class="row attend-name">
                <div class="col-md-2 text-center">&nbsp;
                    @if($value->attand_cond=='A')
                    <input type="checkbox" id="select_{{$i}}" class="single">
                    @elseif($value->attand_cond=='P')
                    <input type="checkbox" checked="" id="select_{{$i}}" class="single">
                    @elseif($value->attand_cond=='H')
                    <input type="checkbox" disabled="" id="select_{{$i}}" class="single">
                    @endif
                </div>
                <div class="col-md-8 text-left">
                    {{$value->id.'/'.$value->admission_id}}
                    <input type="hidden"  name="id[]" value="{{$value->id}}">
                    <input type="hidden"  name="admission_id[]" value="{{$value->admission_id}}">
                    <input type="hidden" id="cond_select_{{$i}}" name="attand_cond[]" value="{{$value->attand_cond}}">
                </div>
                <div class="col-md-1 text-right">
                    @if($value->attand_cond=='A')
                    <span class="badge a-attend-cond" id="text_select_{{$i}}">{{$value->attand_cond}}</span>
                    @elseif($value->attand_cond=='P')
                    <span class="badge p-attend-cond" id="text_select_{{$i}}">{{$value->attand_cond}}</span>
                    @elseif($value->attand_cond=='H')
                    <span class="badge h-attend-cond" id="text_select_{{$i}}">{{$value->attand_cond}}</span>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
        </div>
    </div>
    
    {!! Form::close()!!}
</div><hr/>

@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    @include('partials._msg')
            /*
             * Start code
             */
    $('#session').on('change', function () {
                    $.ajax({
                        url: "attend_index",
                        type: "GET",
                        async: false,
                        data: {'id':$('#session').val()},
                        success: function (data) {
                            var result = '<option value=1>Select</option>';
                            $.each(data, function( index, value ) {
                                
                                    });
                                  
//                                    console.log(result);
                            }
                    });
                });
                /*
                 * 
                 */
                
    $("#selectall").change(function(){
        if(this.checked){
            var i =0;
            $(".single").each(function(){
                 i++;
                this.checked=true;
            var select =$('#select_'+i).attr('id');
                     text = "#text_"+select;
                  cond = "#cond_"+select;
            $(text).text('P');
            $(text).removeClass('a-attend-cond');
             $(text).addClass('p-attend-cond');
            $(cond).attr('value','P');
//            console.log(cond);
            })              
        }else{
            var i =0;
            $(".single").each(function(){
            i++;
            this.checked=false;
            var select =$('#select_'+i).attr('id');
                text = "#text_"+select;
             cond = "#cond_"+select;
            $(text).text('A');
            $(text).removeClass('p-attend-cond');
             $(text).addClass('a-attend-cond');
            $(cond).attr('value','A');
//            console.log(select);
            })              
        }
    });

    $(".single").click(function () {
     var select =$(this).attr('id');
             text = "#text_"+select;
             cond = "#cond_"+select;
//   console.log(cond);
    if ($(this).is(":checked")){
        $(cond).attr('value','P');
        $(text).text('P');
        $(text).removeClass('a-attend-cond');
        $(text).addClass('p-attend-cond');
        
      var isAllChecked = 0;
      
      
      $(".single").each(function(){
        if(!this.checked)
           isAllChecked = 1;
      })              
      if(isAllChecked == 0){ $("#selectall").prop("checked", true); }     
    }
    else {
        $(text).text('A');
        $(text).removeClass('p-attend-cond');
        $(text).addClass('a-attend-cond');
        $(cond).attr('value','A');
      $("#selectall").prop("checked", false);
    }
  });
                /*
                 * 
                 */
   $("#holiday").change(function(){
        if(this.checked){
            var i =0;
            $(".single").each(function(){
                 i++;
                this.checked=false;
            var select =$('#select_'+i).attr('id');
                     text = "#text_"+select;
                  cond = "#cond_"+select;
            $(text).text('H');
            $(text).removeClass('p-attend-cond');
            $(text).removeClass('a-attend-cond');
            $(text).addClass('h-attend-cond');
            $('.single').attr('disabled','');
            $(cond).attr('value','H');
//            console.log($('.single').attr('disabled'));
            })              
        }else{
            var i =0;
            $(".single").each(function(){
            i++;
            this.checked=false;
            var select =$('#select_'+i).attr('id');
                text = "#text_"+select;
             cond = "#cond_"+select;
             $('.single').removeAttr('disabled');
            $(text).text('A');
            $(text).removeClass('h-attend-cond');
            $(text).addClass('a-attend-cond');
            $(cond).attr('value','A');
//            console.log(select);
            })              
        }
    });
    $(document).change(function(){
        var i=0;
            p=0;
            a=0;
         $(".single").each(function(){
             i++;
             var id = $('#text_select_'+i).attr('id');
             if($('#'+id).text()=='A'){
                 a+=1;
             }else if($('#'+id).text()=='P'){
                 p+=1;
             }else if($('#'+id).text()=='H'){
                p=0;
                a=0; 
             }
//             console.log(id);
         });
         $('#p').text(p);
         $('#a').text(a);
        console.log(p);
        console.log(a);
    });
            /*
             * End Code
             */
         
    });
</script>
@endsection






