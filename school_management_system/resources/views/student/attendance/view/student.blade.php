<?php
    $input_date = $date;
    $day = date("d", strtotime($date));
    $month = date("m", strtotime($date));
    $year = date("Y", strtotime($date));
    ?>
<div class="panel panel-primary">
    <div class="panel-heading">
        Current Date&nbsp;<span class="badge">{{date( "d,M-Y", strtotime( $date ) )}}</span>
        <i class="col-lg-offset-5">
            @if($date > \Carbon\Carbon::now())
              <span class="badge">This Date is Advance Date.</span>
            @else
                @if($attend->count()>0)
                    @if($attend->where('attand_cond','H')->count()>0)
                    <span class="badge">Today Is Holiday</span>
                    @else
                    Present &nbsp;&nbsp;<span class="badge">{{$attend->where('attand_cond','P')->count()}}</span>
                &nbsp;&nbsp;Absent &nbsp;&nbsp; <span class="badge">{{$attend->where('attand_cond','A')->count()}}</span>
                    @endif
                @else
                <span class="badge">There are no Student Attendanc This Current Date.</span>
                @endif
            @endif
        </i>
    </div>
  <div class="panel-body">
      <p class="text-center"><span class="badge"> Overall Student ({{date("F-Y", strtotime($date))}})</span></p>
      <div style="overflow: auto;max-height: 250px">
      <table class="table table-sm table-bordered" >
        <tr>
            <th>Students</th>
            @for($i=1;$i<=date("t", strtotime($date));$i++)
            @php($count_attend =\App\student\Attendance::where('date', date( "Y-m-d", strtotime( "$year-$month-$i" ) ))->where('class_id', $class_id)->where('session_id', $session_id)->count())
                @if(date("d", strtotime($date))==$i)
                    @if(date("d", strtotime(\Carbon\Carbon::now()))<$i)
                    <th style="background-color: #888a85;color: #EEE">{{$i}}</th>
                    @else
                        @if($count_attend >0)
                        <th style="background-color: #888a85;color: #EEE">{{$i}}<br/><a style="color: #EEE" href="{{route('attend_view',[date( "Y-m-d", strtotime( "$year-$month-$i" ) ),$session_id,$class_id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></th>
                        @else
                        <th style="background-color: #888a85;color: #EEE">{{$i}}<br/><a style="color: #EEE" href="{{route('attend_create',[date( "Y-m-d", strtotime( "$year-$month-$i" ) ),$session_id,$class_id])}}"><i class="fa fa-plus" aria-hidden="true"></i></a></th>
                        @endif

                    @endif
                @else
                    @if(date("d", strtotime(\Carbon\Carbon::now()))<$i)
                    <th>{{$i}}</th>
                    @else
                        @if($count_attend >0)
                        <th>{{$i}}<br/><a href="{{route('attend_view',[date( "Y-m-d", strtotime( "$year-$month-$i" ) ),$session_id,$class_id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></th>
                        @else
                        <th>{{$i}}<br/><a href="{{route('attend_create',[date( "Y-m-d", strtotime( "$year-$month-$i" ) ),$session_id,$class_id])}}"><i class="fa fa-plus" aria-hidden="true"></i></a></th>
                        @endif

                    @endif
                @endif
            @endfor
            <th>Total</th>
        </tr>
        @foreach($student as $value)
        @php($total =0)
        <tr style="text-align: center;font-weight: bold">
            <td>{{$value->id}}</td>
            @for($i=1;$i<= date("t", strtotime($date));$i++)
            @php( $day =$i)
            <?php
            $makedate = date("Y-m-d", strtotime("$year-$month-$day"));
            $cond = \App\student\Attendance::where('date', $makedate)->where('class_id', $class_id)->where('session_id', $session_id)->where('admission_id', $value->id)->first();
            ?>
            @if($cond['attand_cond']=='P')
            @php($total +=1)
            <td class="p-attend-cond">{{$cond['attand_cond']}}</td>
            @elseif($cond['attand_cond']=='A')
            <td class="a-attend-cond">{{$cond['attand_cond']}}</td>
            @elseif($cond['attand_cond']=='H')
            <td class="h-attend-cond">{{$cond['attand_cond']}}</td>
            @else
            <td class="d-attend-cond">{{$cond['attand_cond']}}</td>
            @endif
            @endfor 
            <td>{{$total}}</td>
        </tr>
        @endforeach
    </table>
      </div>
  </div>
</div>


