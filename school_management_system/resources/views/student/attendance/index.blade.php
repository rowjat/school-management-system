@extends('master')
@section('title','Student')
@section('content')
<div class="page-header">
  <h1>Student Attendance</h1>
</div>
<div class="row">
    {!! Form::open() !!}
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('session_id','Session Year :',['class'=>'col-sm-5 control-label']) !!}
            <div class="col-sm-6">
                @php ($session_arr = ['select'])
                @foreach($session_year as $value)
                @php ($session_arr[$value->id] = $value->name)
                @endforeach
                {!! Form::select('session_id',$session_arr,null,['class'=>'form-control','id'=>'session']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('date','Current Date :',['class'=>'col-sm-5 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::date('date',\Carbon\Carbon::now(),['class'=>'form-control','id'=>'date']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('class_id','Select Class :',['class'=>'col-sm-5 control-label']) !!}
            <div class="col-sm-5">
                <select name="class_id" id="class_id" class="form-control">

                </select>
            </div>
        </div> 
    </div>
    {!! Form::close() !!}
</div><hr/>

<div class="row" id="student">
    
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    @include('partials._msg')
            /*
             * Start code
             */
    $('#session').on('change', function () {
                    $.ajax({
                        url: "attend_index",
                        type: "GET",
                        async: false,
                        data: {'id':$('#session').val()},
                        success: function (data) {
                            var result = '<option value=1>Select</option>';
                            $.each(data, function( index, value ) {
                                result +="<option value='"+value['id']+"'> "+value['name']+"</option>"
                                    });
                                    $("#class_id").append().empty();
                                    $("#class_id").append(result);
//                                    console.log(result);
                            }
                    });
                });
      $('#class_id').on('change', function () {
                    $.ajax({
                        url: "attend_index_1",
                        type: "GET",
                        async: false,
                        data: {
                        'class_id':$('#class_id').val(),
                        'session_id':$('#session').val(),
                        'date':$('#date').val()
                     },
                        success: function (data) {
                                    $("#student").append().empty();
                                    $("#student").append(data);
                                     console.log(data);
                            }
                    });
                });         
  
    $('#date').on('change', function () {
        $("#student").append().empty();
        $('#class_id').val(0);
        
    });


            /*
             * End Code
             */
         
    });
</script>
@endsection



