
<table class="table table-sm table-borderless ">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th class="text-right"></th>
        </tr>

    </thead>
    <tbody >
        @foreach($exam as $value)
        <tr>
            <td >#</td>
            <td>{{$value->name}}</td>
            <td class="text-right"><a href="#" class="btn btn-sm btn-primary">Edit</a></td>
        </tr>
        @endforeach
    </tbody>

</table>