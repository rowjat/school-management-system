@extends('master')
@section('title','Student')
@section('content')
<div class="page-header">
  <h1>Exam </h1>
</div>
<div class="row ">
    <div class="col-md-6">
        <p><a href="#create_exam" class="btn btn-sm btn-primary" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;New Exam</a></p>  
    </div>
    <div class="col-md-6">
        <form class="form-horizontal">
            <div class="form-group">
                <label for="session" class="form-label col-md-2">Session :</label>
                <div class="col col-md-4">
                    <select id="session" name="session_id" class="form-control" placeholder='Select'>
                        @foreach($session_year as $value)
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endforeach
                    </select>
                </div>
                  
            </div>
                
        </form>
    </div>
    
</div>
<div class="row">
    <div class="col col-md-6 col-md-offset-3" id="exam_tbl">
        <table class="table table-sm table-borderless ">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th class="text-right"></th>
        </tr>

    </thead>
    <tbody >
        @foreach($exam as $value)
        <tr>
            <td >#</td>
            <td>{{$value->name}}</td>
            <td class="text-right"><a href="#" class="btn btn-sm btn-primary">Edit</a></td>
        </tr>
        @endforeach
    </tbody>

</table>
    </div>
</div>
    

<!--start model-->
<div class="modal fade" id="create_exam" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">    
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="form-horizontal" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">New Exam</h4>
                </div>
                <div class="modal-body">
                    <span class="alert alert-success col-md-12" id="success-msg">Data have been save successfull</span> 
                    <div class="form-group">
                        <label for="session_id" class="col-sm-4 control-label">Select Session Year :</label>
                        <div class="col-sm-5">
                            @php($session_arr = [])
                            @if($session_model==true)
                            @foreach($session_model as $value)
                            @php($session_arr[$value->id] = $value->name)
                            @endforeach
                            @endif
                            {!!Form::select('session_id',$session_arr,null,['class'=>'form-control','placeholder'=>'Select','id'=>'session_id'])!!}
                            <span class="error-msg" id='error_session_id'></span> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">Name :</label>
                        <div class="col-sm-5">
                            {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'some thing hear','id'=>'name'])!!}
                            <span class="error-msg" id='error_name'></span> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>&nbsp;Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
         $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    @include('partials._msg')
            /*
             * Start code
             */
             $('#success-msg').hide();
 $('form').submit(function (e) {
    
     e.preventDefault();
     var formData = new FormData();
        formData.append('name',$('#name').val());
        formData.append('session_id',$('#session_id').val());
//        console.log(formData);
                    $.ajax({
                        url: "exam_store",
                        method: "POST",
                        processData: false,
                        contentType: false,
                        cache: false,
                        dataType: 'json',
                        data: formData,
                        success: function (data) {
                            if(!data.success){
                             $('#error_session_id').text('');
                             $('#error_session_id').text(data.error['session_id']); 
                                   $('#error_name').text("");
                                   $('#error_name').text(data.error['name']);
                                   $('#success-msg').hide();
                                   
//                                console.log(i);
                            }else{
                                $('#error_session_id').text('');
                                $('#error_name').text("");
                                $('#success-msg').show();
                                $('#success-msg').text(data.msg);
                            }
                             console.log(data);
                            },
                        error:  function () {}
                    });
                });
                $('#session').on('change',function(){
                    $.ajax({
                        url: "exam_index",
                        method: "get",
                        cache: false,
                        data: {
                            'session':$('#session').val()
                        },
                        success: function (data) {
                            $('#exam_tbl').append().empty();
                            $('#exam_tbl').append(data);
                            console.log(data);
                        }
                    });
                });
            /*
             * End Code
             */
    });
</script>
@endsection


