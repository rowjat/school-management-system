@extends('master')
@section('title','Student')
@section('content')
<div class="page-header">
  <h1>Subject </h1>
</div>
<div class="row ">
    <div class="col-md-6">
        <p><a href="#create_subject" class="btn btn-sm btn-primary" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;New Subject</a></p>  
    </div>
    <div class="col-md-6">
        <form class="form-horizontal">
            <div class="form-group">
                <label for="session" class="form-label col-md-2">Session :</label>
                <div class="col col-md-4">
                    <select id="session" name="session_id" class="form-control" placeholder='Select'>
                        <option value="">Select</option>
                        @foreach($session_index as $value)
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
    </div>
    
</div>
<div class="row">
    <div class="col col-md-8 col-md-offset-2" id="subject_tbl" style="overflow: auto;max-height: 250px">
       
    </div>
</div>
    

<!--start model-->
<div class="modal fade" id="create_subject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">    
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="form-horizontal subject" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">New Subject</h4>
                </div>
                <div class="modal-body">
                    <span class="alert alert-success col-md-12" id="success-msg"></span> 
                    <div class="form-group">
                        <label for="session_id" class="col-sm-4 control-label">Select Session Year :</label>
                        <div class="col-sm-5">
                            @php($session_arr = [])
                            @foreach($session_model as $value)
                            @php($session_arr[$value->id]=$value->name)
                            {!!Form::select('session_id',$session_arr,null,['class'=>'form-control','placeholder'=>'Select','id'=>'session_id'])!!}
                            <span class="error-msg" id='error_session_id'></span> 
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="class_id" class="col-sm-4 control-label">Class Name :</label>
                        <div class="col-sm-5">
                            <select name="class_id" id="class_id" class="form-control">
                                <option value=''></option>
                            </select>
                            <span class="error-msg" id='error_class_id'></span> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">Subject Name :</label>
                        <div class="col-sm-5">
                            {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'some thing hear','id'=>'name'])!!}
                            <span class="error-msg" id='error_name'></span> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="total_marks" class="col-sm-4 control-label">Total marks :</label>
                        <div class="col-sm-5">
                            {!!Form::number('total_marks',null,['class'=>'form-control','placeholder'=>'some thing hear','id'=>'total_marks'])!!}
                            <span class="error-msg" id='error_total_marks'></span> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="passing_marks" class="col-sm-4 control-label">Passing Marks :</label>
                        <div class="col-sm-5">
                            {!!Form::number('passing_marks',null,['class'=>'form-control','placeholder'=>'some thing hear','id'=>'passing_marks'])!!}
                            <span class="error-msg" id='error_passing_marks'></span> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>&nbsp;Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
         $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    @include('partials._msg')
            /*
             * Start code
             */
            $('#session_id').on('change',function(){
                $.ajax({
                     url: "subject_index",
                        method: "get",
                        cache: false,
                        data: {
                            'session_id':$('#session_id').val()
                        },
                        success: function (data) {
                            var select = '';
                            select +="<option value=''>Select</option>";
                            $.each(data,function(index , value){
                                select +="<option value='"+value.id+"'>"+value.name+"</option>";
                            });
                                
//                                $('#class_id').append().empty();
                            if(data.length>0){
                            $('#class_id').append().empty();
                            $('#class_id').append(select);
                        }else{
                            $('#class_id').append().empty();
                        }
                            console.log(data);
                        }
                });
            });
            
            
 $('#success-msg').hide();
function errMsg(id , data=null){
    if(data==null){
        $('#error_'+id).text("");
    }else{
      $('#error_'+id).text("");
      $('#error_'+id).text(data);
    }
 }            
 $('form').submit(function (e) {
     e.preventDefault();
     var formData = new FormData();
        formData.append('session_id',$('#session_id').val());
        formData.append('class_id',$('#class_id').val());
        formData.append('name',$('#name').val());
        formData.append('total_marks',$('#total_marks').val());
        formData.append('passing_marks',$('#passing_marks').val());
//        console.log(formData);
                    $.ajax({
                        url: "subject_store",
                        method: "POST",
                        processData: false,
                        contentType: false,
                        cache: false,
                        dataType: 'json',
                        data: formData,
                        success: function (data) {
                            if(!data.success){
                                errMsg('session_id' , data.error['session_id']);
                                errMsg('class_id' , data.error['class_id']);
                                errMsg('name' , data.error['name']);
                                errMsg('total_marks' , data.error['total_marks']);
                                errMsg('passing_marks' , data.error['passing_marks']);
                                   $('#success-msg').hide();
                            }else{
                                errMsg('session_id');
                                errMsg('class_id');
                                errMsg('name');
                                errMsg('total_marks');
                                errMsg('passing_marks');
                                $('#success-msg').show();
                                $('#success-msg').text(data.msg);
                            }
                             console.log(data);
                            },
                        error:  function () {}
                    });
                });
                $('#session').on('change',function(){
                    $.ajax({
                        url: "subject_index_view",
                        method: "get",
                        cache: false,
                        data: {
                            'session':$('#session').val()
                        },
                        success: function (data) {
                            $('#subject_tbl').append().empty();
                            $('#subject_tbl').append(data);
                            console.log(data);
                        }
                    });
                });
            /*
             * End Code
             */
    });
</script>
@endsection


