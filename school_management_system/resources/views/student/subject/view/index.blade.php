 <table class="table table-sm table-bordered  table-striped text-center">
    <thead>
        <tr>
            <th>Class Name</th>
            <th>SL.NO</th>
            <th>Subject Name</th>
            <th>Total Marks</th>
            <th>Passing Markes</th>
            <th>Edit</th>
        </tr>

    </thead>
    <tbody >
        @php($c = 0)
        @foreach($class_Name as $Value)
        @php($subject =\App\student\Subject::where('session_id',$ses)->where('class_id',$Value->id)->get())
        
        @php($s = 0)
        
        @foreach($subject as $subValue)
        @php($s++)
        @if($s==1)
        <tr style="border-top: 2px solid #843534;">
            <th style="vertical-align:middle;background-color: #d5d5d5" rowspan="{{$subject->count()}}">{{$Value->name}}</th>
        @else
        <tr>
        @endif
            <td>{{$s}}</td>
            <td>{{$subValue->name}}</td>
            <td >{{$subValue->total_marks}}</td>
            <td>{{$subValue->passing_marks}}</td>
            <td class="text-right"><a href="#" class="btn btn-sm btn-primary">Edit</a></td>
        </tr>
        @endforeach
        @endforeach
    </tbody>

</table>