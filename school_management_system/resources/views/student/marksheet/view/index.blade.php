<div class="col-lg-6 form-horizontal">
    <div class="form-group">
        <label for="admission_id" class="form-label col-md-3 text-right">Select Student :</label>
        <div class="col-md-6">
            <select name="admission_id" id="admission_id" class="form-control" placeholder='Select'>
                <option value="">Select</option>
                @foreach($admission as $value)
                <option value="{{$value->id}}">{{$value->student_id}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="exam" class="col-md-3 form-label text-right">Type Exam :</label>
        <div class="col-md-6">
            <select name="exam_id" id="exam" class="form-control" placeholder='Select'>
                <option value="">Select</option>
                @foreach($exam as $value)
                <option value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    
</div>
<div class="col-md-6">
    <table class="table" style="border:2px solid #999">
        @foreach($subject as $value) 
        <tr>
            <th>{{$value->name}}</th>
            <td style="width: 100px;height: 12px">
                <input type="hidden" name="subject_id[]" value="{{$value->id}}" id="subject_id">
                <input type="number" name="marks[]" id="marks" class="form-control" style="width: 100px;height: 15px" max="{{$value->total_marks}}"></td>
            <td>{{$value->total_marks}}</td>
            <td>{{$value->passing_marks}}</td>
        </tr>
        @endforeach
    </table>
</div>

