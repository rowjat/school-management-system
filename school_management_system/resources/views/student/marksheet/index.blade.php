@extends('master')
@section('title','Student')
@section('content')
<div class="page-header">
    <h1>Exam Report</h1>
</div>
{!! Form::open(['method'=>'POST','route'=>'marksheet_store','class'=>'']) !!}
<div class="row form-horizontal">
    <div class="col-md-6">
        <div class="form-group">
            <label for="class" class="form-label col-md-3 text-right">Class Name :</label>
            <div class="col-md-6">
                <select name="class" id="class" class="form-control" placeholder='Select'>
                    <option value="">Select</option>
                    @foreach($class_value as $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6" id="save">
    </div>
    </div>   
   <div class="row " id="view">
            
   </div>
       {!!Form::close()!!}
    

@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */
//            $('#save').hide();
            $('#class').on('change',function(){
                $.ajax({
                        url: "marksheet_index",
                        method: "get",
                        cache: false,
                        data: {
                            'class':$('#class').val()
                        },
                        success: function (data) {
                            if(data!=0){
                            $('#view').append().empty();
                            $('#view').append(data);
                            $('#save').append().empty();
                            $('#save').append("<button type='submit' class='btn btn-default'>Save</button>");
                            }else{
                                $('#view').append().empty();
                                $('#save').append().empty();
                            }
                            
                            console.log(data);
                        }
                    });
            });

$('#exam').on('change',function(){
    console.log($('#exam').val());
});


            /*
             * End Code
             */
    });
</script>
@endsection


