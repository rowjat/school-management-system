<!DOCTYPE html>
<html lang="en">

    @include('partials._head')
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        @include('partials._navbar')
        <section  class="" style="padding-top: 100px">
            <hr/>
            <div class="container">
                @yield('content')
            </div>
        </section>
        <!--include('partials._footer')-->
        
        @include('partials._js')
        @yield('javascript')
    </body>
</html>
