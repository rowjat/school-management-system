@extends('master')
@section('title','Student')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Example</h1>
    </div>
</div>

<div class="row">
    <?php
    $x = 6;
    $y = 5;
    $z = $x + $y;
    echo "PHP :" . $z . "<br/>";
    ?>
</div><hr/>
<button  id="click">click</button>
<div class="row">
    <div class="col col-md-6 col-md-offset-3" id="showme">

    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
        var x =6;
            y =5;
            z = x+y;
            $('#click').click(function(){
                $('#click').addClass('btn btn-danger');
                $('#showme').append().empty();
                $('#showme').append('JAVA :'+z);
            });
//           y = $('#showme').text('JAVA :'+z);
//            alert(y);
//            console.log('JAVA :'+z);
       /*
        * End Code
        */
    });
    </script>
@endsection