@extends('master')
@section('title','Setting')
@section('content')
<div class="row">
    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Create New Session</h2></div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','route'=>'session_store','class'=>'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('name','Name Of Session :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::text('name',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('date','Session Start Date :',['class'=>'col-sm-4 control-label']) !!}
                    <div class="col-sm-4">
                        {!! Form::date('date',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group" style="width: ">
                    <div class="col-sm-offset-4 col-sm-10 ">
                        {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                        <a href="{{route('session_index')}}" class="btn btn-danger">Cancle</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
    
<script type="text/javascript">
       @include('partials._msg') 
    </script>
@endsection

