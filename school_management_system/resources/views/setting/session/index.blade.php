@extends('master')
@section('title','Setting')
@section('content')
<div class="row">
    <div class="col-md-12 text-right">
        <a href="session_create" class="btn btn-default">Create new Session Year</a>

    </div>
</div><hr/>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>View All Session Year</h3></div>

            <table class="table table-bordered table-hover">
                <thead style=" background-color: #398439">
                    <tr>
                        <th>#</th>
                        <th>Session Year</th>
                        <th>Start Date</th>
                        <th style="width: 150px">Active</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($session_year as $value)
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$value->name}}</td>
                        <td>{{$value->date}}</td>
                        {!! Form::open(['method'=>'put','route'=>'session_update']) !!}
                <input type="hidden" name="id" value="{{$value->id}}">
                <input type="hidden" name="active" value="{{$value->active=='active' ?'deactive':'active' }}">
                <td>
                    <button type="submit" class="btn btn-sm btn-{{ $value->active == 'active' ? 'success':'danger' }}" ><?php echo $value->active == 'active' ? "<i class='fa fa-check' aria-hidden='true'></i>" : "<i class='fa fa-close' aria-hidden='true'></i>" ?></button>
                    <a href="{{route('session_update',$value->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                </td>
                {!! Form:: close()!!}
                </tr>
                @endforeach
                </tbody>
            </table>
            @if($session_year->render())
            {{$session_year->render()}}
            @endif

        </div>
    </div>
</div>  
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
    @include('partials._msg')
            /*
             * Start code
             */





            /*
             * End Code
             */
    });
</script>
@endsection


