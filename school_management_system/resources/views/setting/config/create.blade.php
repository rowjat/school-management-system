@extends('master')
@section('title','Setting')
@section('content')
<div class="row">
    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>School Configaration Entry form</h2></div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','route'=>'config_store','class'=>'form-horizontal']) !!}
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            {!! Form::label('name','School Name :',['class'=>'col-sm-5 control-label','required'=>'']) !!}
                            <div class="col-sm-7">
                                {!! Form::text('name',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('address','Address :',['class'=>'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                                {!! Form::textarea('address',null,['class'=>'form-control','style'=>'height:35px;']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('tel_num','Tell Number :',['class'=>'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                                {!! Form::text('tel_num',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('email','Email No :',['class'=>'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                                {!! Form::email('email',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                            </div>
                        </div> 
                        <div class="form-group">
                            {!! Form::label('date','Date :',['class'=>'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                                {!! Form::date('date',\Carbon\Carbon::now(),['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('website','Website :',['class'=>'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                                {!! Form::text('website',null,['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('establish_date','Establish date :',['class'=>'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                                {!! Form::date('establish_date',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('logo','Logo :',['class'=>'col-sm-5 control-label']) !!}
                            <div class="col-sm-7">
                                {!! Form::file('logo',null,['class'=>'form-control','data-parsley-required'=>'true']) !!}
                            </div>
                        </div>       
                    </div>
                    <div class="col-sm-offset-1 col-sm-3" style="background-color: #269abc;height: 200px;width: 200px">

                    </div>
                </div>




                <div class="form-group" style="width: ">
                    <div class="col-sm-offset-3 col-sm-10 ">
                        {!! Form::submit('Save',['class'=>'btn btn-default']) !!}
                        <a href="{{route('application_index')}}" class="btn btn-danger">Cancle</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        @include('partials._msg')
       /*
        * Start code
        */ 
        
       /*
        * End Code
        */
    });
    </script>
@endsection



