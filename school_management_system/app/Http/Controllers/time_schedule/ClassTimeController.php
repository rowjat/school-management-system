<?php

namespace App\Http\Controllers\time_schedule;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\time_schedule\Priod;
use App\setting\Session_year;
use App\student\School_class;
use App\staff\Staff;
use App\student\Subject;
use App\time_schedule\Class_time;
use Session;
use App\time_schedule\Holiday;
class ClassTimeController extends Controller
{
    public function index(Request $request){
        $session_year = Session_year::where('active','active')->get();
        if($request->ajax()){
            if($request->i==0){
                $class_id = School_class::where('session_years_id',$request->session_id)->get();
                return response($class_id);
            }elseif ($request->i==1){
                $class_time = Class_time::where('session_id',$request->session_id)
                ->where('class_id',$request->class_id)
                ->get();
                $priod = Priod::all();
                $days = Holiday::where('status',0)->get();
                $view = view("time_schedule.class_time.view.index",compact('priod','days','class_time'));
                return response($view);
            }
        }
        return view('time_schedule.class_time.index', compact('session_year'));
    }

    public function create(Request $request) {
        if ($request->ajax()) {
            if ($request->i == 0) {
                $class_time = School_class::where('session_years_id', $request->session_id)->get();
                $data = [
                    'class_time' => $class_time,
                    'i' => 0
                ];
                return response($data);
            } elseif ($request->i == 1) {
                $subject = Subject::where('session_id', $request->session_id)
                        ->where('class_id', $request->class_id)
                        ->get();
                $data = [
                    'subject' => $subject,
                    'i' => 1
                ];
                return response($data);
            }
        }
        $session_year = Session_year::all();
        $staff = Staff::where('statase', 0)->get();
        $priod = Priod::all();
        return view('time_schedule.class_time.create', compact('priod', 'session_year', 'staff'));
    }
    public function store(Request $request){
        $this->validate($request, [
            'session_id'=>'required',
            'class_id'=>'required',
            'day'=>'required'
        ]);
        if(count($request->priod_id)>0){
                for ($i = 0; $i < count($request->priod_id); $i++) {
                    $priod = Class_time::where('session_id', $request->session_id)
                            ->where('class_id', $request->class_id)
                            ->where('day', $request->day)
                            ->where('priod_id', $request->priod_id[$i])
                            ->get();
                    if($priod->count() >0){
                        
                    $table =Class_time::find($priod->first()->id);
                    $table->session_id = $request->session_id;
                    $table->class_id = $request->class_id;
                    $table->day = $request->day;
                    $table->priod_id = $request->priod_id[$i];
                    $table->subject_id = $request->subject_id[$i];
                    $table->staff_id = $request->staff_id[$i];
                    $table->save();
                    } else {
                        $table = new Class_time();
                    $table->session_id = $request->session_id;
                    $table->class_id = $request->class_id;
                    $table->day = $request->day;
                    $table->priod_id = $request->priod_id[$i];
                    $table->subject_id = $request->subject_id[$i];
                    $table->staff_id = $request->staff_id[$i];
                    $table->save();
                    }
                }
           Session::flash('success','Save Successfull !');
           return redirect()->route('class_time_create'); 
            
        } else {
           Session::flash('fails','Sorry no priod Set !');
           return redirect()->route('class_time_create');
        }
        
    }

}
