<?php

namespace App\Http\Controllers\time_schedule;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\time_schedule\Priod;
use Session;
class PriodController extends Controller
{
    public function index(){
        $priod = Priod::all();
        return view('time_schedule.priod.index', compact("priod"));
    }
    public function create(){
        return view('time_schedule.priod.create');
    }
    public function store(Request $request){
        $this->validate($request, [
            'name'=>'required|unique:priods',
            'from'=>'required|date_format:H:i',
            'to'=>'required|date_format:H:i'
        ]);
        $table = new Priod();
        $table->name = $request->name;
        $table->from = $request->from;
        $table->to = $request->to;
        $table->save();
        Session::flash('success','Save Successfull !');
        return redirect()->route('priod_index'); 
    }
}
