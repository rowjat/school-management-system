<?php

namespace App\Http\Controllers\time_schedule;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\time_schedule\Holiday;
use Session;
class HolidayController extends Controller
{
    public function index(){
        $holiday = Holiday::all();
        if($holiday->count()>0){
            $result = true;
            return view('time_schedule.holiday.index', compact('result','holiday'));
        } else {
            $result = FALSE; 
            return view('time_schedule.holiday.index', compact('result'));
        }
        
    }
    public function store(Request $request){
        if(count($request->day)>0){
            for($i=0;$i<count($request->day);$i++){
                $table = new Holiday();
                $table->day = $request->day[$i];
                $table->status = $request->status[$i];
                $table->save();
            }
            Session::flash('success','Save Successfull !');
           return redirect()->route('holiday_index');
        }
    }
    public function update(Request $request){
        if(count($request->day)>0){
            for($i=0;$i<count($request->day);$i++){
                $table =Holiday::find($request->id[$i]);
                $table->day = $request->day[$i];
                $table->status = $request->status[$i];
                $table->save();
            }
            Session::flash('success','Save Successfull !');
           return redirect()->route('holiday_index');
        }
    }
}
