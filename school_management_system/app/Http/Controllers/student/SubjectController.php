<?php

namespace App\Http\Controllers\student;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\student\School_class;
use App\Http\Controllers\setting\SessionController;
use App\setting\Session_year;
use Illuminate\Support\Facades\Validator;
use App\student\Subject;
class SubjectController extends Controller
{
    public function index(Request $request){
        $session_index = Session_year::all();
        if($request->ajax()){
            $class_name = School_class::where('session_years_id', $request['session_id'])->get(); 
            return response($class_name);
        }
        $session_model = Session_year::where('active','active')->get();
        
        return view('student.subject.index', compact('session_model','session_index'));
    }
    public function index_view(Request $request){
        if($request->ajax()){
            $ses = $request->session;
            $class_Name = School_class::where('session_years_id',$request->session)->get();
            $subject = Subject::where('session_id',$request->session)->get();
            if($class_Name->count()>0){
                $view = view("student.subject.view.index", compact('class_Name','subject','ses'));
                return response($view);
            } else {
                return response('Sorry No Class Found....!');
            }
        }
    }

    public function store(Request $request){
         $vali = Validator::make($request->all(), [
                    'class_id' => 'required',
                    'session_id' => 'required',
                    'name' => 'required',
                    'total_marks' => 'required',
                    'passing_marks' => 'required'
        ]);
        if ($vali->fails()) {
            return response([
                'success' => false,
                'error' => $vali->errors()->toArray()
            ]);
        } else {
            $table = new Subject();
            $table->class_id = $request->class_id;
            $table->session_id = $request->session_id;
            $table->name = $request->name;
            $table->total_marks = $request->total_marks;
            $table->passing_marks = $request->passing_marks;
            $table->save();
            return response([
                'success' => true,
                'msg' => 'Data Havebeen save successfull.'
            ]);
        }
    }
}
