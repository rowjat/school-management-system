<?php

namespace App\Http\Controllers\student;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\setting\Session_year;
use App\student\School_class;
use Session;
class ClassController extends Controller
{
    public function ajax_index(Request $request) {
        if ($request->ajax()) {
            if ($request['session_years_id'] > 0) {
                $data = School_class::where('session_years_id', $request['session_years_id'])->paginate(3);
                if($data->count()>0){
                    $id = $request['session_years_id'];
                $view = view('student.class.view.index_tbl', compact('data', 'id'))->render();
                return response($view);
                }  else {
                    return response('No record Data');
                }
                
            }
        }
    }
    public function ajax_index_page(Request $request) {
        if ($request->ajax()) {
            if ($request['id'] > 0) {
                $data = School_class::where('session_years_id', $request['id'])->paginate(3);
                $view = view('student.class.view.index_tbl', compact('data'))->render();
                return response($view);
            }
        }
    }

    public function index(){
        
        $session_years = Session_year::where('active','active')->get();
        return view('student.class.index',compact(['session_years']));
    }

    public function create(){
        $session_year = Session_year::where('active','active')->get();
        return view('student.class.create',compact(['session_year']));
    }
    public function store(Request $request){
        $this->validate($request, [
            'name'=>'required'
        ]);
       $session_year = Session_year::find($request->session_years_id);
       if($session_year->active== 'active'){
           $school_class =School_class::where('session_years_id',$request->session_years_id)->where('name',$request->name)->get();
           if($school_class->count() > 0){
               Session::flash('fails','This Class Allready Taken!');
           return redirect()->route('class_create'); 
           }  else {
             $table = new School_class();
           $table->session_years_id = $request->session_years_id;
           $table->name = $request->name;
           $table->save();
           Session::flash('success','Save Successfull !');
           return redirect()->route('class_index');   
           }
       }  else {
           Session::flash('fails','At first set active Session Year');
           return redirect()->route('class_create'); 
       }
       
    }
    public function edit($id){
    }
}
