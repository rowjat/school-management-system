<?php

namespace App\Http\Controllers\student;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\student\Marksheet;
use App\Http\Controllers\setting\SessionController;
use App\student\School_class;
use App\admission\Admission;
use App\student\Exam;
use App\student\Subject;
use Session;
class MarksheetController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
             if($request->class>0){
             $subject = Subject::where('class_id',$request->class)
                     ->where('session_id',SessionController::view_current_session()['id'])
                     ->get();   
            $admission = Admission::where('school_class_id',$request->class)->get();
            $exam = Exam::where('session_id', SessionController::view_current_session()['id'])->get();
            $view = view('student.marksheet.view.index', compact('admission','exam','subject'));
             } else {
                $view = 0;
             }
           return response($view);
        }
        $class_value = School_class::where('session_years_id', SessionController::view_current_session()['id'])->get();
        return view('student.marksheet.index', compact('class_value'));
    }
    public function store(Request $request){
        $this->validate($request, [
            'admission_id'=>'required',
            'exam_id'=>'required',
            'subject_id'=>'required'
        ]);
        
        if($request->subject_id >0){
            for($i=0;$i<count($request->subject_id);$i++){
                $count = Marksheet::where('admission_id',$request->admission_id)
                ->where('exam_id',$request->exam_id)
                ->where('subject_id',$request->subject_id[$i])
                ->count();
                if($count>0){
                    Session::flash('success', 'Allready Taken!');
                    return redirect()->route('marksheet_index');
                } else {
                    $table = new Marksheet();
                $table->admission_id = $request->admission_id;
                $table->exam_id = $request->exam_id;
                $table->subject_id = $request->subject_id[$i];
                $table->marks = $request->marks[$i];
                $table->save();
                }
            }
            Session::flash('success', 'Save Successfull..........!');
            return redirect()->route('marksheet_index'); 
        } else {
            Session::flash('success', 'There are no subject this Class..........!');
            return redirect()->route('marksheet_index');
        }
    }
}
