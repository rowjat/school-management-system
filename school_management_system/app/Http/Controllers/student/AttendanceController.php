<?php

namespace App\Http\Controllers\student;

use Illuminate\Http\Request;
use App\setting\Session_year;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\setting\SessionController;
use App\student\School_class;
use App\admission\Admission;
use App\student\Attendance;
use Session;
class AttendanceController extends Controller
{
    public function index(Request $request){
        
        $session_year = Session_year::where('active','active')->get();
        if($request->ajax()){
            $class_id = School_class::where('session_years_id',$request['id'])->get();
            return $class_id;
        }
        return view('student.attendance.index', compact(['session_year']));
    }
    public function index_1(Request $request){
        if($request->ajax()){
            $date = $request['date'];
            $session_id = $request['session_id'];
            $class_id = $request['class_id'];
            $student = Admission::where('session_id',$request['session_id'])
                                ->where('school_class_id',$request['class_id'])
                                ->get();
            $attend = Attendance::where('session_id',$request['session_id'])
                    ->where('class_id',$request['class_id'])
                    ->where('date',$request['date'])
                    ->get();
            if($student->count()>0){
                $view = view('student.attendance.view.student', compact(['date','student','attend','class_id','session_id']));
                return response($view);
            } else {
                return response("<p class='alert alert-danger'>This Class No student Taken</p>");
            }
        }
    }

    public function getCreate($date , $session_id , $class_id){
        
    }

    public function create($date , $session_id , $class_id){
        $get_session= Session_year::find($session_id);
        $get_class_id = School_class::find($class_id);
        $get_date = $date;
        $student = Admission::where('session_id',$session_id)
                                ->where('school_class_id',$class_id)
                                ->get();
        if($student->count()>0){
                return view('student.attendance.create', compact(['date','student','get_session','get_class_id']));
            }
    }
    
    public function store(Request $request){
        $input=$request->all();
        $attend = Attendance::where('class_id',$request->class_id)
                ->where('session_id',$request->session_id)
                ->where('date',$request->date)
                ->get();
        if($attend->count()>0){
           Session::flash('fails', 'This Day Allready Taken..........!');
        return redirect()->route('attend_index');  
        }else{
           for ($i = 0; $i < count($input['admission_id']); $i++) {
            $table = new Attendance();
            $table->class_id = $input['class_id'];
            $table->session_id = $input['session_id'];
            $table->date = $input['date'];
            $table->admission_id =$input['admission_id'][$i];
            $table->attand_cond =$input['attand_cond'][$i];
            $table->save();
        }
        Session::flash('success', 'Save Successfull..........!');
        return redirect()->route('attend_index'); 
        }
         
    }
    public function view($date , $session_id , $class_id){
        $get_session= Session_year::find($session_id);
        $get_class_id = School_class::find($class_id);
        $get_date = $date;
        $student = Attendance::where('session_id',$session_id)
                                ->where('class_id',$class_id)
                                ->where('date',$date)
                                ->get();
        if($student->count()>0){
                return view('student.attendance.view', compact(['date','student','get_session','get_class_id']));
            }
    }
    public function update(Request $request) {
        $input = $request->all();
        if ($input['id'] > 0) {
            for ($i = 0; $i < count($input['id']); $i++) {
                $table = Attendance::find($input['id'][$i]);
                $table->class_id = $input['class_id'];
                $table->session_id = $input['session_id'];
                $table->date = $input['date'];
                $table->admission_id = $input['admission_id'][$i];
                $table->attand_cond = $input['attand_cond'][$i];
                $table->save();
            }
            Session::flash('success', 'Save Successfull..........!');
            return redirect()->route('attend_index');
        }
    }

}
