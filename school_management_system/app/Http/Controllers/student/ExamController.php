<?php

namespace App\Http\Controllers\student;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\setting\Session_year;
use App\student\Exam;
use App\Http\Controllers\setting\SessionController;

class ExamController extends Controller
{
    public function index(Request $request){
        $session_year = Session_year::all();
        $session_model = Session_year::where('active','active')->get();
        if($request->ajax()){
            $exam = Exam::where('session_id',$request['session'])->get();
            if($exam->count()>0){
                $view = view('student.exam.view.index', compact(['exam','session_model']));
            } else {
              $view  ='Sorry no Data found..!';
            }
            return response($view);
        }else{
            
            $exam = Exam::where('session_id', SessionController::view_current_session()['id'])->get();
            return view('student.exam.index', compact(['session_year','exam','session_model']));
        }
        
    }
    public function store(Request $request) {
        $vali = Validator::make($request->all(), [
                    'name' => 'required',
                    'session_id' => 'required'
        ]);
        if ($vali->fails()) {
            return response([
                'success' => false,
                'error' => $vali->errors()->toArray()
            ]);
        } else {
            $table = new Exam();
            $table->session_id = $request->session_id;
            $table->name = $request->name;
            $table->save();
            return response([
                'success' => true,
                'msg' => 'Data Havebeen save successfull.'
            ]);
        }
    }

}
