<?php

namespace App\Http\Controllers\admission;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\admission\Student;
use App\admission\Application;
use App\setting\Session_year;
use App\fee\Feehead;
use Session;
use App\admission\Admission;
use App\student\Attendance;
use App\Http\Controllers\setting\SessionController;
use App\student\Exam;
use App\student\Subject;
use App\student\Marksheet;
use App\fee\Fee_collaction;
class StudentController extends Controller
{
    public function index(){
        
    }

    public function create(){
        return view('admission.student.create');
    }
    public function store(Request $request){
        $this->validate($request, [
            'application_id'=>'required',
            'name'=>'required|max:20|min:3',
            'f_name'=>'required|max:20|min:3',
            'm_name'=>'required|max:20|min:3',
            'birth_date'=>'required',
            'gender'=>'required',
            'date'=>'required',
            'school_class_id'=>'required',
            'contact_no'=>'required',
            'email'=>'required|email|max:255',
            'address'=>'required',
            'birth_place'=>'required',
            'nationality'=>'required',
            'religion'=>'required',
              'pictur'=>'required|mimes:jpg,jpeg|Max:100'  
        ]);
        if ($request->file('pictur')) {
            $destinationPath = 'uploads'; // upload path
            $extension = $request->file('pictur')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
            $request->file('pictur')->move($destinationPath, $fileName); // uploading file to given path
        
            $table = new Student();
            $table->application_id = $request->application_id;
            $table->name = $request->name;
            $table->f_name = $request->f_name;
            $table->m_name = $request->m_name;
            $table->birth_date = $request->birth_date;
            $table->gender = $request->gender;
            $table->date = $request->date;
            $table->school_class_id = $request->school_class_id;
            $table->contact_no = $request->contact_no;
            $table->email = $request->email;
            $table->address = $request->address;
            $table->birth_place = $request->birth_place;
            $table->nationality = $request->nationality;
            $table->religion = $request->religion;
            $table->pictur = $fileName;
            $table->save();
            
            $app = Application::find($request->application_id);    
            $app->select          = 'admitted';
            $app->save();
            $session_year_id = Session_year::where('active','active')->first();
            $fee = Feehead::where('school_class_id',$request->school_class_id)->where('session_year_id',$session_year_id->id)->get();            
            Session::flash('fees', $fee);
            Session::flash('reg', $table->id);
            Session::flash('class_id', $request->school_class_id);
            return redirect()->route('admission_create');
            
            
        } else {
            Session::flash('fails', 'uploaded file is not valid');
            return redirect()->route('application_index');
        }
//        endfile
        
    }
    public function view(Request $request){
        if($request->ajax()){
            if($request->i=='student'){
                $student = Admission::find($request->id);
                $view = view('admission.admission.view.student', compact('student'));
            }elseif($request->i=='attend'){
                $month = SessionController::view_current_session()['date'];
                $student = Admission::find($request->id);
                $attend = Attendance::where('session_id', SessionController::view_current_session()['id'])
                        ->where('admission_id',$request->id)->get();
                $view = view('admission.admission.view.attendance', compact('student','attend','month'));
            }elseif($request->i=='exam'){
                $exam = Exam::where('session_id', SessionController::view_current_session()['id'])->get();
                $student = Admission::find($request->id);
                $view = view('admission.admission.view.exam', compact('student','exam'));
            }elseif($request->i=='fee'){
                $student = Admission::find($request->id);
                $month = SessionController::view_current_session()['date'];
                $fee_collection = Fee_collaction::where('admission_id',$request->id)->get();
                $feehead = Feehead::where('session_year_id', SessionController::view_current_session()['id'])
                        ->where('school_class_id',$student->school_class_id)->get();
                $view = view('admission.admission.view.fee', compact('student','month','feehead','fee_collection'));
            }elseif($request->i=='exam_id'){
                $class_id = Admission::find($request->admission_id);
                $subject = Subject::where('session_id', SessionController::view_current_session()['id'])
                        ->where('class_id',$class_id->school_class_id)->get();
                
                $mark_sheet = Marksheet::where('exam_id',$request->id)
                        ->where('admission_id',$request->admission_id)->get();
                if($request->id >0){
                    $view = view('admission.admission.view.exam_tbl', compact('subject','mark_sheet'));
                } else {
                    $view = "<h6 style='color: #bababa;font-size: 80px'>Select Exam</h6>";
                }
                
            }
              
            return response($view);
        }
    }
}
