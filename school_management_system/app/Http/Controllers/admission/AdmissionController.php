<?php

namespace App\Http\Controllers\admission;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\admission\Admission;
use App\fee\FeeSet;
use Session;
use App\setting\Session_year;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\setting\SessionController;
class AdmissionController extends Controller
{
    public function index(){
        $session_id = SessionController::view_current_session()['id'];
            $admissions = Admission::where('session_id',$session_id)->paginate(10);
        return view('admission.admission.index', compact('admissions'))->render();
        
    }
    public function create(){
        return view('admission.admission.create');
    }
    public function store(Request $request){
        $this->validate($request, [
//            'student_id'=>'required',
//            'date'=>'required',
//            'session_year_id'=>'required',
//            'school_class_id'=>'required',
//            
//            
//            'feehead_id'=>'required',
//            'amount'=>'required'
        ]);
       $admission = new Admission();
        $admission->student_id=$request->student_id;
        $admission->date=$request->date;
        $admission->session_id=$request->session_year_id;
        $admission->school_class_id=$request->school_class_id;
        $admission->save();
        $input = $request->all();
        if(isset($input['feehead_id'])){
        for ($i = 0; $i < count($input['feehead_id']); $i++) {
            $fee_set = new FeeSet();
            $fee_set->admission_id = $admission->id;
            $fee_set->feehead_id = $input['feehead_id'][$i];
            $fee_set->amount = $input['amount'][$i];
            $fee_set->save();
        }
        }
        $fee = FeeSet::where('admission_id',$admission->id)->get();
        $data=[
            'admission'=>$admission->id,
            'date'=>$admission->date,
            'session_id'=>$admission->session_id,
            'class_id'=>$admission->school_class_id,
            'fee'=>$fee
        ];
        return view('admission.admission.view')->withData($data);
//        return Redirect::route('admission_view');
    }
    public function details(){
      
        return view('admission.admission.view')->withFee($fee);
    }
    public function pdf($id){
     $fee = FeeSet::where('admission_id',$id)->get();
     $adm = Admission::find($id);
        $data=[
            'admission'=>$adm->id,
            'date'=>$adm->date,
            'session_id'=>$adm->session_id,
            'class_id'=>$adm->school_class_id,
            'fee'=>$fee
        ];
        return $data;
//        return view('admission.admission.view')->withFee($data);
    }
}
