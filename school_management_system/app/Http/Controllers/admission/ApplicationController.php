<?php

namespace App\Http\Controllers\admission;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\setting\Session_year;
use App\student\School_class;
use Session;
use App\admission\Application;
use App\Http\Controllers\setting\SessionController;

class ApplicationController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            $session_year = Session_year::where('active','active')->first();
             $apps = Application::where('session_year_id',$session_year->id)->where($request['tbl_name'],'like','%'.$request['valu'].'%')->paginate(15);
            if($apps->count()>0){
                $view = view('admission.application.view.index',compact('apps'))->render();
                return response($view);
            }else{
           $view ="";
           $view .="<tr><td colspan='9'>! ...have no search</td></tr>";
        return response($view);
            } 
        }  else {
            if(SessionController::view_current_session()!=null ){
                 $apps = Application::where('session_year_id',SessionController::view_current_session()['id'])->paginate(15);
                 
        return view('admission.application.index',compact('apps'))->render();
            }
       return view('admission.application.index');
        }
    }

    public function create(){
        $session_years = Session_year::where('active','active')->get();
        return view('admission.application.create',compact('session_years'));
    }
    public function create_1(Request $request){
        if ($request->ajax()) {
            if($request['id'] >0){
                $data =  School_class::where('session_years_id',$request['id'])->get();
          return response($data);
            }
        }
    }
    public function store(Request $request){
        $this->validate($request, [
            'name'=>'required',
            'contact'=>'required',
            'email'=>'required|email|max:255',
            'date'=>'required',
            'session_year_id'=>'required',
            'school_class_id'=>'required',
            'amount'=>'required',
            'select'=>''
        ]);
        $table = new Application();
        $table->name            = $request->name;
        $table->contact         = $request->contact;
        $table->email           = $request->email;
        $table->date            = $request->date;
        $table->session_year_id = $request->session_year_id;
        $table->school_class_id = $request->school_class_id;
        $table->amount          = $request->amount;
        $table->select          = "";
        $table->save();
        Session::flash('success','Save Successfull !');
           return redirect()->route('application_index'); 
        
    }
    public function edit($id){
        
    }

    public function update(Request $request){
//        $id = $request->id;
        $table = Application::find($request->id);
         if($request->select!='admitted'){   
         $table->select          = $request->select;
         $table->save();
         Session::flash('success','Save Successfull !');
           return redirect()->route('application_index');
         }  else {
            Session::flash('id',$table->id);  
           Session::flash('school_classes',$table->school_class_id);
           return redirect()->route('student_create');
         }
          
    }
}
