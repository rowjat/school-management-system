<?php

namespace App\Http\Controllers\setting;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\setting\Session_year;
use Session;
class SessionController extends Controller
{
    public static function view_current_session(){
        if(Session_year::where('active','active')->count() > 0){
           $value = Session_year::where('active','active')->first();
          return ['id'=>$value->id,'name'=>$value->name,'date'=>$value->date];
        } else {
          return ['id'=>0,'name'=>'','date'=>""];
        }
    }

    public function index(){
        $session_year = Session_year::orderBy('id', 'dasc')->paginate(5);
        return view('setting.session.index', compact(['session_year'])); 
    }

    public function create(){
        return view('setting.session.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'name' =>'required|unique:session_years',
            'date' =>'required',
            ]);
    if(Session_year::where('active','active')->count() >0){
            $session_year = Session_year::where('active','active')->first();
             $table = Session_year::find($session_year->id);
            $table->active = 'deactive';
            $table->save();
        }
        $table = new Session_year();
        $table->name = $request->name;
        $table->date = $request->date;
        $table->active = 'active';
        $table->save();
        Session::flash('success','Save Successfull !');
        return redirect()->route('session_index'); 
    }
    
    public function update(Request $request) {
        if(Session_year::where('active','active')->count() >0){
            $session_year = Session_year::where('active','active')->first();
             $table = Session_year::find($session_year->id);
            $table->active = 'deactive';
            $table->save();
        }
            $table = Session_year::find($request->id);
            $table->active = $request->active;
            $table->save();
            Session::flash('success', 'Save Successfull !');
            return redirect()->route('session_index');
        
    }

}
