<?php

namespace App\Http\Controllers\fee;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\setting\Session_year;
use App\fee\Feehead;
use App\student\School_class;
use Session;

class FeeheadController extends Controller
{
    public function index(){
        
    }
    public function create(){
        $session_years = Session_year::where('active','active')->get();
        return view('fee.feehead.create', compact('session_years')); 
    }
    public function create_1(Request $request){
        if($request->ajax()){
            $date = School_class::where('session_years_id',$request['id'])->get();
            if($date->count()>0){
                return $date;
            }else{
                return '';
            }
        }
        $session_years = Session_year::where('active','active')->get();
        return view('fee.feehead.create', compact('session_years')); 
    }
    public function store(Request $request){
        $this->validate($request, [
           'session_year_id'=>'', 
            'school_class_id'=>'', 
            'name'=>'required', 
            'amount'=>'required|numeric', 
            'collection_type'=>'', 
        ],[
            'name.required' => 'The name field is required.',
            'amount.required' => 'The amount field is required.',
            ]);
        $table = new Feehead();
        $table->session_year_id = $request->session_year_id;
        $table->school_class_id = $request->school_class_id;
        $table->name = $request->name;
        $table->amount = $request->amount;
        $table->collection_type = $request->collection_type;
        $table->save();
         Session::flash('success', $table->id);
            return redirect()->route('feehead_create');
    }
}
