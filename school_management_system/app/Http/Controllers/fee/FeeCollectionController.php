<?php

namespace App\Http\Controllers\fee;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\student\School_class;
use App\Http\Controllers\setting\SessionController;
use App\admission\Admission;
use App\fee\FeeSet;
use App\acount\Receipt;
use App\fee\Fee_collaction;
use Session;
use App\acount\Profit;
class FeeCollectionController extends Controller
{
    public function create(Request $request) {
        if ($request->ajax()) {
            if ($request->i == 0) {
                $admission = Admission::where('session_id', SessionController::view_current_session()['id'])
                        ->where('school_class_id', $request->class_id)
                        ->get();
                return response($admission);
            } elseif ($request->i == 1) {
                $fee_set = FeeSet::where('admission_id',$request->admission_id)
                        ->get();
                $month = SessionController::view_current_session()['date'];
                $collection = Fee_collaction::where('admission_id',$request->admission_id)
                        ->get();
                $view = view("fee.fee_collection.view.create", compact('fee_set','month','collection'));
                return response($view);
            }
        }
        $profit = Profit::all()->max('receipt_id');
        $class_id = School_class::where('session_years_id', SessionController::view_current_session()['id'])->get();
        return view('fee.fee_collection.create', compact('class_id','profit'));
    }
    public function store(Request $request) {
//        return $request->all();

     $this->validate($request, [
            'admission_id' => 'required',
            'date' => 'required',
            'receipt_no' => 'required'
        ]);
      
       
        if (count($request->fee_head_id) > 0) {
            for ($j = 0; $j < count($request->fee_head_id); $j++) {
                
                if (count($request->month[$j]) > 0) {
                    for ($i = 0; $i < count($request->month[$j]); $i++) {
                        if($request->amount[$j][$i]>0){
                            $table = new Fee_collaction();
                            $table->admission_id = $request->admission_id;
                            $table->fee_head_id = $request->fee_head_id[$j];
                            $table->date = $request->date;
                            $table->month = $request->month[$j][$i];
                            $table->amount = $request->amount[$j][$i];
                            $table->save();

                            $receipt = new Receipt();
                            $receipt->receipt_no = $request->receipt_no;
                            $receipt->date = $request->date;
                            $receipt->session_year_id = SessionController::view_current_session()['id'];
                            $receipt->admission_id = $request->admission_id;
                            $receipt->feehead_id = $request->fee_head_id[$j];
                            $receipt->month = $request->month[$j][$i];
                            $receipt->amount = $request->amount[$j][$i];
                            $receipt->type_of_receipt = 1;
                            $receipt->save(); 
                            
                           
                        }
                    }
                }
            }
        }
        $sum_recipt = Receipt::where('receipt_no',$request->receipt_no)->get();
        if($sum_recipt->count()>0){
        $profit = new Profit();
        $profit->receipt_id = $request->receipt_no;
        $profit->amount = $sum_recipt->sum('amount');
        $profit->save();
        }   
        
        Session::flash('success', 'Data have been save successfull');
        return redirect()->route('fee_collection_create');
    }

}
            