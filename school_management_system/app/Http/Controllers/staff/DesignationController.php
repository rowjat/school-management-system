<?php

namespace App\Http\Controllers\staff;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\staff\Designation;
use Session;
class DesignationController extends Controller
{
    public function store(Request $request){
        $this->validate($request, [
            'name'=>'required|unique:designations'
        ]);
        $table = new Designation();
        $table->name = $request->name;
        $table->save();
        Session::flash('success','Data save successfull');
        return redirect()->route('staff_create');
    }
}
