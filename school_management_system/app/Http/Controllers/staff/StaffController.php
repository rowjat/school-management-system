<?php

namespace App\Http\Controllers\staff;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\staff\Staff;
use App\staff\Designation;
use Session;
class StaffController extends Controller
{
    public function index(){
        $staff = Staff::all();
        return view('staff.staff.index', compact('staff'));
    }
    public function create(){
        $desig = Designation::all();
        return view('staff.staff.create', compact('desig'));
    }
    public function store(Request $request){
        $this->validate($request,[
                    'name' => 'required',
                    'f_name' => 'required',
                    'm_name' => 'required',
                    'gender' => 'required',
                    'dob' => 'required',
                    'nationality' => 'required',
                    'contact_no' => 'required|unique:staff',
                    'email' => 'required|email|unique:staff',
                    'address' => 'required',
                    'degignation_id' => 'required',
                    'join_date' => 'required',
                    'pictur' => 'required',
        ]); 
            if ($request->file('pictur')) {
            $destinationPath = 'uploads\staff'; // upload path
            $extension = $request->file('pictur')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
            $request->file('pictur')->move($destinationPath, $fileName); // uploading file to given path
             $table = new Staff();
             $table->name = $request->name;
             $table->f_name = $request->f_name;
             $table->m_name = $request->m_name;
             $table->gender = $request->gender;
             $table->dob = $request->dob;
             $table->nationality = $request->nationality;
             $table->contact_no = $request->contact_no;
             $table->email = $request->email;
             $table->address = $request->address;
             $table->degignation_id = $request->degignation_id;
             $table->join_date = $request->join_date;
             $table->statase = 0;
             $table->releas_date = "";
             $table->pictur = $fileName;
             $table->save();
             Session::flash('success','Data save successfull');
             return redirect()->route('staff_index');  
            
            }else{
              Session::flash('fails','Data save successfull');
              return redirect()->route('staff_create');  
            }
    }
}
