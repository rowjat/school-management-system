<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','IndexController@index');
/*
 * setting Route
 * config
 */
Route::get('config_create',[
    'uses' => 'setting\ConfigController@create',
    'as'   => 'config_create'
]);
Route::post('config_store',[
    'uses' => 'setting\ConfigController@store',
    'as'   => 'config_store'
]);
Route::get('config_index',[
    'uses' => 'setting\ConfigController@index',
    'as'   => 'config_index'
]);
Route::PUT('config_update',[
    'uses' => 'setting\ConfigController@update',
    'as'   => 'config_update'
]);
/*
 * setting Route
 * session
 */
Route::get('session_create',[
    'uses' => 'setting\SessionController@create',
    'as'   => 'session_create'
]);
Route::post('session_store',[
    'uses' => 'setting\SessionController@store',
    'as'   => 'session_store'
]);
Route::get('session_index',[
    'uses' => 'setting\SessionController@index',
    'as'   => 'session_index'
]);
Route::PUT('session_update',[
    'uses' => 'setting\SessionController@update',
    'as'   => 'session_update'
]);
/*
 * student Route
 */
Route::get('class_index',[
    'uses' => 'student\ClassController@index',
    'as'   => 'class_index'
]);
Route::get('class_create',[
    'uses' => 'student\ClassController@create',
    'as'   => 'class_create'
]);
Route::Post('class_store',[
    'uses' => 'student\ClassController@store',
    'as'   => 'class_store'
]);
Route::get('ajax_class_index',[
    'uses' => 'student\ClassController@ajax_index',
    'as'   => 'ajax_class_index'
]);
Route::get('ajax_class_index_page',[
    'uses' => 'student\ClassController@ajax_index_page',
    'as'   => 'ajax_class_index_page'
]);
/*
 * Student
 * Attendance
 */
Route::get('attend_index',[
    'uses' => 'student\AttendanceController@index',
    'as'   => 'attend_index'
]);
Route::get('attend_index_1',[
    'uses' => 'student\AttendanceController@index_1',
    'as'   => 'attend_index_1'
]);
Route::get('attend_create/{date}/{session_id}/{class_id}',[
    'uses' => 'student\AttendanceController@create',
    'as'   => 'attend_create'
]);
Route::get('attend_view/{date}/{session_id}/{class_id}',[
    'uses' => 'student\AttendanceController@view',
    'as'   => 'attend_view'
]);
Route::POST('attend_store',[
    'uses' => 'student\AttendanceController@store',
    'as'   => 'attend_store'
]);
Route::PUT('attend_update',[
    'uses' => 'student\AttendanceController@update',
    'as'   => 'attend_update'
]);
/*
 * Student
 * Exam
 */
Route::get('exam_index',[
    'uses' => 'student\ExamController@index',
    'as'   => 'exam_index'
]);
Route::POST('exam_store',[
    'uses' => 'student\ExamController@store',
    'as'   => 'exam_store'
]);
/*
 * Student
 * subject
 */
Route::get('subject_index',[
    'uses' => 'student\SubjectController@index',
    'as'   => 'subject_index'
]);
Route::get('subject_index_view',[
    'uses' => 'student\SubjectController@index_view',
    'as'   => 'subject_index_view'
]);
Route::POST('subject_store',[
    'uses' => 'student\SubjectController@store',
    'as'   => 'subject_store'
]);
/*
 * Student
 * Marksheet
 */
Route::get('marksheet_index',[
    'uses' => 'student\MarksheetController@index',
    'as'   => 'marksheet_index'
]);
Route::POST('marksheet_store',[
    'uses' => 'student\MarksheetController@store',
    'as'   => 'marksheet_store'
]);
/*
 * Admission
 * Application
 */
Route::get('application_index',[
    'uses' => 'admission\ApplicationController@index',
    'as'   => 'application_index'
]);
Route::get('application_create',[
    'uses' => 'admission\ApplicationController@create',
    'as'   => 'application_create'
]);
Route::get('application_create_1',[
    'uses' => 'admission\ApplicationController@create_1',
    'as'   => 'application_create_1'
]);
Route::Post('application_store',[
    'uses' => 'admission\ApplicationController@store',
    'as'   => 'application_store'
]);
Route::get('application_edit/{id}',[
    'uses' => 'admission\ApplicationController@edit',
    'as'   => 'application_edit'
]);
Route::PUT('application_update',[
    'uses' => 'admission\ApplicationController@update',
    'as'   => 'application_update'
]);
/*
 * Admission
 * Student
 */
Route::get('student_index',[
    'uses' => 'admission\StudentController@index',
    'as'   => 'student_index'
]);
Route::get('student_create',[
    'uses' => 'admission\StudentController@create',
    'as'   => 'student_create'
]);
Route::POST('student_store',[
    'uses' => 'admission\StudentController@store',
    'as'   => 'student_store'
]);
Route::get('student_view',[
    'uses' => 'admission\StudentController@view',
    'as'   => 'student_view'
]);
/*
 * Admission
 * admission
 */
Route::get('admission_index',[
    'uses' => 'admission\AdmissionController@index',
    'as'   => 'admission_index'
]);
Route::get('admission_create',[
    'uses' => 'admission\AdmissionController@create',
    'as'   => 'admission_create'
]);
Route::POST('admission_store',[
    'uses' => 'admission\AdmissionController@store',
    'as'   => 'admission_store'
]);
Route::get('/admission_view',[
    'uses' => 'admission\AdmissionController@details',
    'as'   => 'admission_view'
]);
Route::get('/admission_pdf/{data}',[
    'uses' => 'admission\AdmissionController@pdf',
    'as'   => 'admission_pdf'
]);
/*
 * Staff
 * staff
 */
Route::get('staff_index',[
    'uses' => 'staff\StaffController@index',
    'as'   => 'staff_index'
]);
Route::get('staff_create',[
    'uses' => 'staff\StaffController@create',
    'as'   => 'staff_create'
]);
Route::POST('staff_store',[
    'uses' => 'staff\StaffController@store',
    'as'   => 'staff_store'
]);
/*
 * Staff
 * desig
 */
Route::get('desig_index',[
    'uses' => 'staff\DesignationController@index',
    'as'   => 'desig_index'
]);
Route::get('desig_create',[
    'uses' => 'staff\DesignationController@create',
    'as'   => 'desig_create'
]);
Route::POST('desig_store',[
    'uses' => 'staff\DesignationController@store',
    'as'   => 'desig_store'
]);
/*
 * Time_Schedule
 * class_time
 */
Route::get('class_time_index',[
    'uses' => 'time_schedule\ClassTimeController@index',
    'as'   => 'class_time_index'
]);
Route::get('class_time_create',[
    'uses' => 'time_schedule\ClassTimeController@create',
    'as'   => 'class_time_create'
]);
Route::POST('class_time_store',[
    'uses' => 'time_schedule\ClassTimeController@store',
    'as'   => 'class_time_store'
]);
/*
 * Time_Schedule
 * holiday
 */
Route::get('holiday_index',[
    'uses' => 'time_schedule\HolidayController@index',
    'as'   => 'holiday_index'
]);
Route::get('holiday_create',[
    'uses' => 'time_schedule\HolidayController@create',
    'as'   => 'holiday_create'
]);
Route::POST('holiday_store',[
    'uses' => 'time_schedule\HolidayController@store',
    'as'   => 'holiday_store'
]);
Route::PUT('holiday_update',[
    'uses' => 'time_schedule\HolidayController@update',
    'as'   => 'holiday_update'
]);

/*
 * Time_Schedule
 * Priod
 */
Route::get('priod_index',[
    'uses' => 'time_schedule\PriodController@index',
    'as'   => 'priod_index'
]);
Route::get('priod_create',[
    'uses' => 'time_schedule\PriodController@create',
    'as'   => 'priod_create'
]);
Route::POST('priod_store',[
    'uses' => 'time_schedule\PriodController@store',
    'as'   => 'priod_store'
]);
/*
 * Fee
 * feehead
 */
Route::get('feehead_index',[
    'uses' => 'fee\FeeheadController@index',
    'as'   => 'feehead_index'
]);
Route::get('feehead_create',[
    'uses' => 'fee\FeeheadController@create',
    'as'   => 'feehead_create'
]);
Route::get('feehead_create_1',[
    'uses' => 'fee\FeeheadController@create_1',
    'as'   => 'feehead_create_1'
]);
Route::POST('feehead_store',[
    'uses' => 'fee\FeeheadController@store',
    'as'   => 'feehead_store'
]);
/*
 * Fee
 * fee_collection
 */
Route::get('fee_collection_index',[
    'uses' => 'fee\FeeCollectionController@index',
    'as'   => 'fee_collection_index'
]);
Route::get('fee_collection_create',[
    'uses' => 'fee\FeeCollectionController@create',
    'as'   => 'fee_collection_create'
]);
Route::POST('fee_collection_store',[
    'uses' => 'fee\FeeCollectionController@store',
    'as'   => 'fee_collection_store'
]);