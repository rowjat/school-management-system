<?php

namespace App\setting;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    public function applications(){
        return $this->hasMany('App\admition\Application');
    }
}
