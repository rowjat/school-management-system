<?php

namespace App\admission;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    public function student(){
       return $this->belongsTo('App\admission\Student','student_id');
    }
}
