<?php

namespace App\admission;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function admission(){
       return $this->hasMany('App\admission\Admission');
    }
}
