<?php

namespace App\admission;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public function session_years(){
        return $this->belongsTo('App\setting\Session_year','session_year_id');
    }
}
