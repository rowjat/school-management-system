<?php

namespace App\fee;

use Illuminate\Database\Eloquent\Model;

class FeeSet extends Model
{
    public function feehead(){
       return $this->belongsTo('App\fee\Feehead','feehead_id');
    }
}
