<?php

namespace App\fee;

use Illuminate\Database\Eloquent\Model;

class Feehead extends Model
{

     public function fee_sets(){
       return $this->hasMany('App\fee\FeeSet');
    }
     public function fee_collactions(){
       return $this->hasMany('App\fee\Fee_collaction');
    }
}
